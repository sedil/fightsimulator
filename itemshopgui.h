#ifndef ITEMSHOPGUI_H
#define ITEMSHOPGUI_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QListWidget>

#include "player.h"

namespace fightsim {

/**
 * @brief The ItemShop class
 * Provides an itemshop where items can be purchased or inventaritems
 * from players can be sold
 */
class ItemshopGUI : public QWidget {
private:
    Player *_player;
    QList<ItemObject*> *_shopitemlist;
    QLabel *_moneyLabel;
    QLabel *_money;
    QPushButton *_buy;
    QPushButton *_sell;
    QLabel *_itemdescription;
    QListWidget *_inventarView;
    QListWidget *_shopitemView;

    void updateListitems();
    void initComponents();

    Q_OBJECT
public:
    explicit ItemshopGUI(Player *player, QList<ItemObject*> *shopitems, QWidget *parent = nullptr);
    virtual ~ItemshopGUI();

public slots:
    void showItemDescription(bool);
    void sellItem();
    void buyItem();
};

}

#endif // ITEMSHOPGUI_H
