#include "fightmenu.h"
#include "player.h"

#include <QBoxLayout>

namespace fightsim {

Fightmenu::Fightmenu(Fight *fight, Actor *actor, QList<Actor*> *targets, QWidget *parent)
    : QWidget{parent}, _fight(fight), _active(actor), _targets(targets), _physAttack(false), _magicAttack(false), _itemUsage(false) {
    initComponents();
}

Fightmenu::~Fightmenu() { }

void Fightmenu::initComponents(){
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight);

    QBoxLayout *buttonlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    _combo = new QPushButton("Combo");
    _magic = new QPushButton("Magic");
    _item = new QPushButton("Item");
    buttonlayout->addWidget(new QLabel(_active->getName()));
    buttonlayout->addWidget(_combo);
    buttonlayout->addWidget(_magic);

    if(_active->accessMagicSlot()->getSpelllist().size() == 0){
        _magic->setEnabled(false);
    }

    uint consumitems = 0;
    for(ItemObject *item : *Player::inventar->getInventar()){
        if(item->getType() == Itemtype::CONSUMABLE){
            consumitems++;
        }
    }

    if(consumitems == 0){
        _item->setEnabled(false);
    }

    if(!_active->isOpponent()){
        buttonlayout->addWidget(_item);
    }

    QBoxLayout *rightlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QBoxLayout *desclayout = new QBoxLayout(QBoxLayout::LeftToRight);
    _objectdescription = new QLabel();
    desclayout->addWidget(_objectdescription);

    QBoxLayout *listlayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout *targetlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    _targetlist = new QListWidget();
    _targetlist->hide();
    _confirm = new QPushButton("OK");
    _confirm->hide();
    targetlayout->addWidget(_targetlist);
    targetlayout->addWidget(_confirm);

    QBoxLayout *objectlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    _objectlist = new QListWidget();
    _objectlist->hide();
    objectlayout->addWidget(_objectlist);

    rightlayout->addLayout(desclayout);
    listlayout->addLayout(targetlayout);
    listlayout->addLayout(objectlayout);

    rightlayout->addLayout(listlayout);
    layout->addLayout(buttonlayout);
    layout->addLayout(rightlayout);

    connect(_combo,&QPushButton::clicked,this,[this]{combo();});
    connect(_magic,&QPushButton::clicked,this,[this]{magic();});
    connect(_item,&QPushButton::clicked,this,[this]{item();});
    connect(_confirm,&QPushButton::clicked,this,[this]{confirm();});
    connect(_objectlist,&QListWidget::currentRowChanged,this,[this]{updateObjectdescription();});

    setLayout(layout);
    setWindowTitle("Choose Attack and Target");
    setMinimumSize(480,180);
    setWindowModality(Qt::WindowModality::ApplicationModal);
    show();
}

// SLOTS //

void Fightmenu::updateObjectdescription(){
    QString objectname = _objectlist->currentItem()->text();
    /* if magicbutton was clicked, then only magicspells will be listed */
    if(_magicAttack){
        for(const ItemObject *object : _active->accessMagicSlot()->getSpelllist()){
            if(objectname == object->getItemname()){
                _objectdescription->setText(object->getItemdescription());
                break;
            }
        }
    }
    /* if itembutton was clicked, only items will be listed */
    if(_itemUsage){
        for(const ItemObject *object : *Player::inventar->getInventar()){
            if(objectname == object->getItemname()){
                _objectdescription->setText(object->getItemdescription());
                break;
            }
        }
    }
}

void Fightmenu::combo(){
    _physAttack = true;
    _targetlist->show();
    _confirm->show();

    for(const Actor *actor : *_targets){
        if(actor->isAlive()){
            _targetlist->addItem(actor->getName());
        }
    }
    _targetlist->setCurrentRow(0);

    _combo->setEnabled(false);
    _magic->setEnabled(false);
    _item->setEnabled(false);
}

void Fightmenu::magic(){
    _magicAttack = true;
    _targetlist->show();

    for(const Actor *actor : *_targets){
        if(actor->isAlive()){
            _targetlist->addItem(actor->getName());
        }
    }
    _targetlist->setCurrentRow(0);

    _objectlist->show();
    for(const MagicItem *magic : _active->accessMagicSlot()->getSpelllist()){
        _objectlist->addItem(magic->getItemname());
    }
    _objectlist->setCurrentRow(0);

    QString magicname = _objectlist->currentItem()->text();
    for(const MagicItem *magic : _active->accessMagicSlot()->getSpelllist()){
        if(magicname == magic->getItemname()){
            _objectdescription->setText(magic->getItemdescription());
            break;
        }
    }

    _confirm->show();
    _combo->setEnabled(false);
    _magic->setEnabled(false);
    _item->setEnabled(false);
}

void Fightmenu::item(){
    _itemUsage = true;
    _targetlist->show();

    for(const Actor *actor : *_targets){
        if(actor->isAlive()){
            _targetlist->addItem(actor->getName());
        }
    }
    _targetlist->setCurrentRow(0);

    _objectlist->show();
    for(const ItemObject *item : *Player::inventar->getInventar()){
        if(item->getType() == Itemtype::CONSUMABLE){
            _objectlist->addItem(item->getItemname());
        }
    }
    _objectlist->setCurrentRow(0);

    QString itemname = _objectlist->currentItem()->text();
    for(const ItemObject *item : *Player::inventar->getInventar()){
        if(itemname == item->getItemname()){
            _objectdescription->setText(item->getItemdescription());
            break;
        }
    }

    _confirm->show();
    _combo->setEnabled(false);
    _magic->setEnabled(false);
    _item->setEnabled(false);
}

void Fightmenu::confirm(){
    uint index = _targetlist->currentRow();
    const QString targetname = _targetlist->takeItem(index)->text();
    for(Actor *target : *_targets){
        if(targetname == target->getName() && _physAttack){
            _fight->setIndexForGUI(index);
            _fight->doPhysicAttack(_active,target);
            break;
        }
        /* player 2 and enemies cannot use magic, crash */
        else if (targetname == target->getName() && _magicAttack){
            const QString magicname = _objectlist->takeItem(_objectlist->currentRow())->text();
            for(MagicItem *spell : _active->accessMagicSlot()->getSpelllist()){
                if(magicname == spell->getItemname()){
                    _fight->setIndexForGUI(index);
                    _fight->doMagicAttack(_active,spell,index);
                    break;
                }
            }
            break;
        }
        /* last item use, crash */
        else if (targetname == target->getName() && _itemUsage){
            const QString itemname = _objectlist->takeItem(_objectlist->currentRow())->text();
            for(ItemObject *item : *Player::inventar->getInventar()){
                if(itemname == item->getItemname()){
                    _fight->setIndexForGUI(index);
                    _fight->useItem(dynamic_cast<ConsumItem*>(item),target);
                    break;
                }
            }
            break;
        }
    }
    close();
}

}
