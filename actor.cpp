#include "actor.h"

namespace fightsim {

    Actor::Actor(QString name, ushort level, ushort hp, ushort mp, ushort str, ushort vit, ushort agi, short phys, short fire, short wind, short water, short earth, bool opponentFlag) :
    _name(name), _level(level), _actHP(hp), _maxHP(hp), _actMP(mp), _maxMP(mp), _strength(str),
    _vitality(vit), _agility(agi), _physRes(phys), _fireRes(fire), _windRes(wind), _waterRes(water), _earthRes(earth), _isOpponent(opponentFlag) {
        _fightAttributes = QMap<QString, short>();
        _fightAttributes.insert("ATK",0);
        _fightAttributes.insert("DEF",0);
        _fightAttributes.insert("ACT",0);

        _statusAilment = QMap<Ailment, bool>();
        _statusAilment.insert(Ailment::POISON, false);
        _statusAilment.insert(Ailment::PARALYZE, false);
        _statusAilment.insert(Ailment::MAGIC_BLOCK, false);

        _paralyzeeffectCounter = 3;
        _magicblockCounter = 5;

        _spells = new MagicSlot();
        _isAlive = true;
    }

    Actor::~Actor() {
        delete _spells;
    }

    QString Actor::getName() const {
        return _name;
    }

    ushort Actor::getLevel() const {
        return _level;
    }

    ushort Actor::getStrength() const {
        return _strength;
    }

    ushort Actor::getVitality() const {
        return _vitality;
    }

    ushort Actor::getAgility() const {
        return _agility;
    }

    /**
     * @brief Actor::changeHP changes actual HP of an actor because
     * of damage or healing
     * @param hp changes the actual HP value
     */
    void Actor::changeHP(short hp) {
        if(hp <= 0){
            if(_actHP + hp <= 0){
                _actHP = 0;
                setAliveFlag(false);
            } else {
                _actHP += hp;
            }
        } else {
            if(_actHP + hp > _maxHP){
                _actHP = _maxHP;
            } else {
                _actHP += hp;
            }
        }
    }

    /**
     * @brief Actor::changeMP changes actual MP of an actor if a magic
     * spell was casted
     * @param mp changes the actual mp value
     */
    void Actor::changeMP(short mp) {
        if(mp < 0){
            if(_actMP + mp < 0){
                // nicht genug MP
            } else if(_actMP + mp >= 0){
                ushort temp = _actMP + mp;
                _actMP = temp;
            }
        } else{
            if(_actMP + mp > _maxMP){
                _actMP = _maxMP;
            }
            _actMP += mp;
        }
    }

    /**
     * @brief Actor::setAliveFlag sets a flag if this actor lives or not
     * @param alive true, lifes or false if this actor should be death
     */
    void Actor::setAliveFlag(bool alive){
        _isAlive = alive;
    }

    /**
     * @brief Actor::changeAttribute changes fightattributes during fight
     * @param key kind of attribute what should be changed
     * @param value -1 decreades an attribute, 1 increases an attribute
     */
    void Actor::changeAttribute(QString key, short value) {
        if(key == "ATK" && value == -1){
            if(_fightAttributes[key] > -1){
                short temp = _fightAttributes[key] - 1;
                _fightAttributes[key] = temp;
            }
        } else if(key == "ATK" && value == 1){
            if(_fightAttributes[key] < 1){
                short temp = _fightAttributes[key] + 1;
                _fightAttributes[key] = temp;
            }
        } else if(key == "DEF" && value == -1){
            if(_fightAttributes[key] > -1){
                short temp = _fightAttributes[key] - 1;
                _fightAttributes[key] = temp;
            }
        } else if(key == "DEF" && value == 1){
            if(_fightAttributes[key] < 1){
                short temp = _fightAttributes[key] + 1;
                _fightAttributes[key] = temp;
            }
        } else if(key == "ACT" && value == -1){
            if(_fightAttributes[key] > -1){
                short temp = _fightAttributes[key] - 1;
                _fightAttributes[key] = temp;
            }
        } else if(key == "ACT" && value == 1){
            if(_fightAttributes[key] < 1){
                short temp = _fightAttributes[key] + 1;
                _fightAttributes[key] = temp;
            }
        }
    }

    /**
     * @brief Actor::changeAilmentFlag changes an ailment flag which effects during
     * battle
     * @param ailment kind of ailment
     * @param flag true, ailment is activated or false
     */
    void Actor::changeAilmentFlag(Ailment ailment, bool flag){
        switch(ailment){
        case Ailment::POISON:
            _statusAilment[ailment] = flag;
            break;
        case Ailment::PARALYZE:
            _statusAilment[ailment] = flag;
            break;
        case Ailment::MAGIC_BLOCK:
            _statusAilment[ailment] = flag;
            break;
        }
    }

    /**
     * @brief Actor::paralyzeeffectCounter decreases paralyzecounter. If value reaches 0, actor is
     * healed from paralyze
     */
    void Actor::paralyzeeffectCounter(){
        if(_paralyzeeffectCounter > 0){
            _paralyzeeffectCounter--;
        } else {
            _statusAilment[Ailment::PARALYZE] = false;
            _paralyzeeffectCounter = 3;
        }
    }

    /**
     * @brief Actor::magicblockCounter decreases magicblockcounter. If value is 0, magicblock is deactivted
     */
    void Actor::magicblockCounter(){
        if(_magicblockCounter > 0){
            _magicblockCounter--;
        } else {
            _statusAilment[Ailment::MAGIC_BLOCK] = false;
            _magicblockCounter = 5;
        }
    }

    ushort Actor::getActualHP() const {
        return _actHP;
    }

    ushort Actor::getMaximumHP() const {
        return _maxHP;
    }

    ushort Actor::getActualMP() const {
        return _actMP;
    }

    ushort Actor::getMaximumMP() const {
        return _maxMP;
    }

    bool Actor::isOpponent() const {
        return _isOpponent;
    }

    bool Actor::isAlive() const {
        return _isAlive;
    }

    short Actor::getPhysResistance() const {
        return _physRes;
    }

    short Actor::getFireResistance() const {
        return _fireRes;
    }

    short Actor::getWindResistance() const {
        return _windRes;
    }

    short Actor::getWaterResistance() const {
        return _waterRes;
    }

    short Actor::getEarthResistance() const {
        return _earthRes;
    }

    short Actor::getAttribute(QString key) const {
        return _fightAttributes[key];
    }

    bool Actor::getAilmentFlag(Ailment ailment) const {
        return _statusAilment[ailment];
    }

    /**
     * @brief Actor::accessMagicSlot
     * @return magicslot of this actor
     */
    MagicSlot* Actor::accessMagicSlot() const {
        return _spells;
    }

}
