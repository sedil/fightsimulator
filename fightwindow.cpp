#include "fightwindow.h"
#include "fightmenu.h"
#include <QLabel>

namespace fightsim {

Fightwindow::Fightwindow(QList<Player*> *players, QList<NPC*> *opponents, QWidget *parent)
    : QWidget{parent}, _player(players), _npcs(opponents) {
    initComponents();
    _fight = new Fight(this, *players, *opponents);
}

Fightwindow::~Fightwindow(){
    for(Actor *actor : *_player){
        delete actor;
    }
    for(Actor *actor : *_npcs){
        delete actor;
    }
}

void Fightwindow::initComponents(){
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    QGridLayout *opponentgrid = new QGridLayout();
    for(auto n = 0; n < _npcs->size(); n++){
        opponentgrid->addItem(createStatuspanel(_npcs->at(n)), n, 0);
    }

    QBoxLayout *fightgrid = new QBoxLayout(QBoxLayout::LeftToRight);
    _fightinfo = new QTextEdit();
    _fightinfo->setReadOnly(true);

    QBoxLayout *buttonlayout = new QBoxLayout(QBoxLayout::TopToBottom);
    _confirm = new QPushButton("Fight");
    _abort = new QPushButton("Abort/End");

    buttonlayout->addWidget(_confirm);
    buttonlayout->addWidget(_abort);

    fightgrid->addWidget(_fightinfo);
    fightgrid->addLayout(buttonlayout);

    QGridLayout *playergrid = new QGridLayout();
    for(auto n = 0; n < _player->size(); n++){
        playergrid->addItem(createStatuspanel(_player->at(n)), n, 0);
    }

    layout->addLayout(opponentgrid);
    layout->addLayout(fightgrid);
    layout->addLayout(playergrid);

    connect(_confirm,&QPushButton::clicked,this,[this]{commandInput();});
    connect(_abort,&QPushButton::clicked,this,[this]{abort();});

    setLayout(layout);
    setWindowTitle("Battle");
    setMinimumSize(640,360);
    setWindowModality(Qt::WindowModality::ApplicationModal);
    show();
}

QGridLayout* Fightwindow::createStatuspanel(const Actor *actor){
    QGridLayout *info = new QGridLayout();

    QBoxLayout *status = new QBoxLayout(QBoxLayout::LeftToRight);
    status->addWidget(new QLabel(""));
    status->addWidget(new QLabel(""));
    status->addWidget(new QLabel(""));

    info->addLayout(status, 0, 0, 0, 1);

    QBoxLayout *actordata = new QBoxLayout(QBoxLayout::LeftToRight);
    actordata->addWidget(new QLabel(actor->getName()));
    actordata->addWidget(new QLabel("HP"));
    actordata->addWidget(new QLabel(QString::number(actor->getActualHP()) + " / " + QString::number(actor->getMaximumHP())));
    actordata->addWidget(new QLabel("MP"));
    actordata->addWidget(new QLabel(QString::number(actor->getActualMP()) + " / " + QString::number(actor->getMaximumMP())));

    info->addLayout(actordata, 0, 1, 0, 4);
    return info;
}

/**
 * @brief Fightwindow::updateActorpanel updates the panel of the specific actor
 * @param actor updates this actorlabels
 * @param targetindex index for the grid within the panel
 */
void Fightwindow::updateActorpanel(Actor *actor, uint targetindex){
    uint gridindex = 2;
    if(actor->isOpponent()){
        gridindex = 0;
    }

    QGridLayout *grid = dynamic_cast<QGridLayout*>(layout()->itemAt(gridindex)); // 0 = opponent, 2 = player
    QGridLayout *innergrid = dynamic_cast<QGridLayout*>(grid->itemAtPosition(targetindex,0)); // pos = p1-p3 oder e1-e8
    QBoxLayout *status = dynamic_cast<QBoxLayout*>(innergrid->itemAt(0)); // status
    QLabel *magicsap = dynamic_cast<QLabel*>(status->itemAt(0)->widget());
    QLabel *para = dynamic_cast<QLabel*>(status->itemAt(1)->widget());
    QLabel *poison = dynamic_cast<QLabel*>(status->itemAt(2)->widget());

    if(actor->getAilmentFlag(Ailment::MAGIC_BLOCK)){
        magicsap->setText("MAG");
    } else {
        magicsap->clear();
    }

    if(actor->getAilmentFlag(Ailment::PARALYZE)){
        para->setText("PAR");
    } else {
        para->clear();
    }

    if(actor->getAilmentFlag(Ailment::POISON)){
        poison->setText("POZ");
    } else {
        poison->clear();
    }

    QBoxLayout *data = dynamic_cast<QBoxLayout*>(innergrid->itemAt(1)); // info
    QLabel *hp = dynamic_cast<QLabel*>(data->itemAt(2)->widget());
    QLabel *mp = dynamic_cast<QLabel*>(data->itemAt(4)->widget());

    hp->setText(QString::number(actor->getActualHP())+ " / " + QString::number(actor->getMaximumHP()));
    mp->setText(QString::number(actor->getActualMP())+ " / " + QString::number(actor->getMaximumMP()));
}

void Fightwindow::updateFightinfo(const QString &text){
    _fightinfo->append(text);
}

void Fightwindow::fightEnd(){
    _confirm->setEnabled(false);
}

// SLOTS //

void Fightwindow::commandInput(){
    Actor *actor = _fight->getActiveActor();
    QList<Actor*> *actors = new QList<Actor*>();
    if(actor->isOpponent()){
        for(Player *p : *_player){
            actors->append(p);
        }
        new Fightmenu(_fight,_fight->getActiveActor(),actors);
    } else {
        /* specialcase: Items can be used for players and opponents */
        for(NPC *npc : *_npcs){
            actors->append(npc);
        }
        new Fightmenu(_fight,_fight->getActiveActor(),actors);
    }
}

void Fightwindow::abort(){
    close();
}

}
