#include "manaegg.h"

namespace fightsim {

    ManaEgg::ManaEgg() : ItemObject("Dust Egg","",Itemtype::EGG, 0, QPair<Effect,short>(Effect::NO_EFFECT,0)),
        _fireboost(0), _windboost(0), _waterboost(0), _earthboost(0) { }

    ManaEgg::ManaEgg(QString name, QString desc, ushort fireboost, ushort windboost, ushort waterboost, ushort earthboost, ushort value)
        : ItemObject(name,desc,Itemtype::EGG,value,QPair<Effect,short>(Effect::NO_EFFECT,0)),
          _fireboost(fireboost), _windboost(windboost), _waterboost(waterboost), _earthboost(earthboost){ }

    ManaEgg::ManaEgg(const ManaEgg &copy)
        : ItemObject(copy.getItemname(),copy.getItemdescription(),Itemtype::EGG,copy.getValue(),copy.getAttribute()),
          _fireboost(copy.getFireboostLevel()), _windboost(copy.getWindboostLevel()), _waterboost(copy.getWaterboostLevel()), _earthboost(copy.getEarthboostLevel()) { }

    ManaEgg::~ManaEgg(){}

    ushort ManaEgg::getFireboostLevel() const {
        return _fireboost;
    }

    ushort ManaEgg::getWindboostLevel() const {
        return _windboost;
    }

    ushort ManaEgg::getWaterboostLevel() const {
        return _waterboost;
    }

    ushort ManaEgg::getEarthboostLevel() const {
        return _earthboost;
    }

}
