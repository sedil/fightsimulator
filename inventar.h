#ifndef INVENTAR_H
#define INVENTAR_H

#include <QObject>
#include "itemobject.h"

namespace fightsim {

    /**
     * @brief The Inventar class
     * This is the players inventar. All players share this inventar
     */
    class Inventar {
    private:
        QList<ItemObject*> _itemlist;
    public:
        explicit Inventar();
        virtual ~Inventar();

        void addItem(ItemObject *item);
        void deleteItem(QString itemname);
        ItemObject* getItemCopy(uint index);
        ItemObject* getItem(uint index);
        QList<ItemObject*>* filterItems(Itemtype itemtype);
        QList<ItemObject*>* getInventar();

    };

}

#endif // INVENTAR_H
