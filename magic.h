#ifndef MAGIC_H
#define MAGIC_H

#include "itemobject.h"

namespace fightsim {

    /**
     * @brief The MagicItem class A subclass of itemobject
     */
    class MagicItem : public ItemObject {
    private:
        /* type of magic like FIRE, WIND, AILMENT, ... */
        Magictype _magictype;

        /* area of effect: Hits all actors */
        bool _isAOE;
        ushort _basedamage;

        /* the need of MP for this magicspell */
        ushort _mpUsage;
    public:
        explicit MagicItem();
        explicit MagicItem(QString name, QString desc, Magictype magictype, ushort value, ushort basedamage, ushort mpUsage, bool isAOE, QPair<Effect, short> attribute);
        explicit MagicItem(const MagicItem &copy);
        virtual ~MagicItem();

        Magictype getMagicType() const;
        bool isAOE() const;
        ushort getMPUsage() const;
        ushort getBasedamage() const;
        MagicItem* getSpell();

    };

}

#endif // MAGIC_H
