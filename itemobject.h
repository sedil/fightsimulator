#ifndef ITEMOBJECT_H
#define ITEMOBJECT_H

#include <QObject>
#include "values.h"

namespace fightsim {

    /**
     * @brief The ItemObject class
     * Baseclass of differet types of items
     */
    class ItemObject {
    private:
        QString _name;
        QString _description;
        Itemtype _type;
        ushort _value;
        /* an item can have attributes like damage with a value */
        QPair<Effect, short> _attribute;
    public:
        explicit ItemObject(QString name, QString description, Itemtype type, ushort value, QPair<Effect, short> attribute);
        virtual ~ItemObject();

        QString getItemname() const;
        virtual QString getItemdescription() const;
        Itemtype getType() const;
        ushort getValue() const;
        QPair<Effect, short> getAttribute() const;
    };
}

#endif // ITEMOBJECT_H
