#include "gear.h"

namespace fightsim {

    GearItem::GearItem() : ItemObject("","",Itemtype::GEAR,0,QPair<Effect,ushort>(Effect::NO_EFFECT,0)) { }

    GearItem::GearItem(QString name, QString desc, Geartype gear, ushort value, QPair<Effect, short> attribute) :
        ItemObject(name,desc,Itemtype::GEAR,value,attribute), _geartype(gear) { }

    GearItem::GearItem(const GearItem &copy) :
        ItemObject(copy.getItemname(),copy.getItemdescription(),copy.getType(),copy.getValue(),copy.getAttribute()),
        _geartype(copy.getGearType()) { }

    GearItem::~GearItem() { }

    /**
     * @brief GearItem::getGearType
     * @return type of gear
     */
    Geartype GearItem::getGearType() const {
        return _geartype;
    }

}
