#ifndef GEARSLOT_H
#define GEARSLOT_H

#include <QObject>
#include "gear.h"
#include "manaegg.h"
#include "player.h"

namespace fightsim {

    class Player;

    /**
     * @brief The GearSlot class
     * A class which represents the "body" of the actor
     */
    class GearSlot {
    private:
        Player *_player;
        GearItem* _head;
        GearItem* _chest;
        GearItem* _foot;
        GearItem* _accessoire;
        ManaEgg* _manaegg;
        GearItem* _weapon;

        void calculateAttribute(GearItem &gear, bool unequip);
    public:
        explicit GearSlot(Player *player);
        virtual ~GearSlot();

        void initHeadgear(GearItem *head);
        void initChestgear(GearItem *chest);
        void initFootgear(GearItem *foot);
        void initAccessoire(GearItem *accessoire);
        void initManaEgg(ManaEgg *manaegg);
        void initWeapon(GearItem *weapon);

        GearItem* showHeadgear();
        GearItem* showChestgear();
        GearItem* showFootgear();
        GearItem* showAccessoire();
        ManaEgg* showManaegg();
        GearItem* showWeapon();

        GearItem* equipHeadgear(GearItem* head);
        GearItem* equipChestgear(GearItem* chest);
        GearItem* equipFootgear(GearItem* foot);
        GearItem* equipAccessoire(GearItem* accessoire);
        ManaEgg* equipManaegg(ManaEgg* manaegg);
        GearItem* equipWeapon(GearItem* weapon);

        GearItem* unequipHeadgear();
        GearItem* unequipChestgear();
        GearItem* unequipFootgear();
        GearItem* unequipAccessoire();
        ManaEgg* unequipManaegg();
        GearItem* unequipWeapon();
    };

}

#endif // GEARSLOT_H
