#ifndef EQUIPWINDOW_H
#define EQUIPWINDOW_H

#include <QObject>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QComboBox>
#include <QListWidget>
#include "player.h"
#include "itemobject.h"

namespace fightsim {

/**
 * @brief The EquipWindow class
 * This window helps the player to equip gear and magicspells
 */
class EquipWindow : public QWidget {
private:
    Q_OBJECT

    Player *_player;

    QLabel *_headlabel;
    QLabel *_chestlabel;
    QLabel *_footlabel;
    QLabel *_accessoirelabel;
    QLabel *_weaponlabel;
    QLabel *_manaegglabel;

    QComboBox *_head;
    QComboBox *_chest;
    QComboBox *_foot;
    QComboBox *_accessoire;
    QComboBox *_weapon;
    QComboBox *_manaegg;

    QPushButton *_removeSpell;
    QListWidget *_activeSpells;

    QPushButton *_addSpell;
    QListWidget *_availableSpells;

    void initComponents();
    void fillCombobox();
    void updateMagicspelllist();

public:
    explicit EquipWindow(Player *player, QWidget *parent = nullptr);
    virtual ~EquipWindow();

public slots:
    void equipHeadgear();
    void equipChestgear();
    void equipFootgear();
    void equipAccessoire();
    void equipWeapon();
    void equipManaegg();
    void removeSpellFromList();
    void addSpellToList();

};

}

#endif // EQUIPWINDOW_H
