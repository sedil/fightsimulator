#include "magic.h"

namespace fightsim {

    MagicItem::MagicItem() :
        ItemObject("","",Itemtype::MAGICSPELL,0,QPair<Effect,ushort>(Effect::NO_EFFECT,0)){
        _magictype = Magictype::NO_ELEM;
        _isAOE = false;
    }

    MagicItem::MagicItem(QString name, QString desc, Magictype magictype, ushort value, ushort basedamage, ushort mpUsage, bool isAOE, QPair<Effect, short> attribute) :
        ItemObject(name,desc,Itemtype::MAGICSPELL,value,attribute), _magictype(magictype), _isAOE(isAOE), _basedamage(basedamage), _mpUsage(mpUsage){ }

    MagicItem::MagicItem(const MagicItem &copy) :
        ItemObject(copy.getItemname(),copy.getItemdescription(),copy.getType(),copy.getValue(),copy.getAttribute()),
        _magictype(copy.getMagicType()), _isAOE(copy.isAOE()), _basedamage(copy.getBasedamage()), _mpUsage(copy.getMPUsage()) { }

    MagicItem::~MagicItem() { }

    /**
     * @brief MagicItem::getMagicType
     * @return type of this spell
     */
    Magictype MagicItem::getMagicType() const {
        return _magictype;
    }

    /**
     * @brief MagicItem::getBasedamage
     * @return basedamage which this spell causes
     */
    ushort MagicItem::getBasedamage() const {
        return _basedamage;
    }

    /**
     * @brief MagicItem::getMPUsage
     * @return the need of Manapoints for this spell
     */
    ushort MagicItem::getMPUsage() const {
        return _mpUsage;
    }

    /**
     * @brief MagicItem::isAOE
     * @return true if this is an area of effect spell
     */
    bool MagicItem::isAOE() const {
        return _isAOE;
    }

    MagicItem* MagicItem::getSpell(){
        return this;
    }

}
