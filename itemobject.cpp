#include "itemobject.h"

namespace fightsim {

    ItemObject::ItemObject(QString name, QString description, Itemtype type, ushort value, QPair<Effect, short> attribute) :
    _name(name), _description(description), _type(type), _value(value), _attribute(attribute){ }

    ItemObject::~ItemObject() { }

    QString ItemObject::getItemname() const {
        return _name;
    }

    QString ItemObject::getItemdescription() const {
        return _description;
    }

    /**
     * @brief ItemObject::getType
     * @return return the type of this item like Magicspell, Consumitem
     */
    Itemtype ItemObject::getType() const {
        return _type;
    }

    /**
     * @brief ItemObject::getValue
     * @return returns the value associates with this item
     */
    ushort ItemObject::getValue() const {
        return _value;
    }

    /**
     * @brief ItemObject::getAttribute
     * @return first attribute shows what this item does and second attribute is
     * the associated value
     */
    QPair<Effect, short> ItemObject::getAttribute() const {
        return _attribute;
    }

}
