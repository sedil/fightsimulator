#ifndef CONSUM_H
#define CONSUM_H

#include "itemobject.h"

namespace fightsim {

    /**
     * @brief The ConsumItem class subclass of itemobject
     */
    class ConsumItem : public ItemObject {
    public:
        explicit ConsumItem();
        explicit ConsumItem(QString name, QString desc, ushort value, QPair<Effect, short> attribute);
        explicit ConsumItem(const ConsumItem &copy);
        virtual ~ConsumItem();
    };

}

#endif // CONSUM_H
