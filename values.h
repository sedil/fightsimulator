#ifndef VALUES_H
#define VALUES_H

namespace fightsim {

    /**
     * @brief The Itemtype enum
     * types of items
     */
    enum Itemtype {
        NO_ITEM = 0,
        CONSUMABLE = 1,
        MAGICSPELL = 2,
        GEAR = 3,
        EGG = 4
    };

    /**
     * @brief The Effect enum
     * kind of effect for magicspells, gear and items
     */
    enum Effect {
        NO_EFFECT = 0,
        REVIVE = 1,
        HEAL_HP = 2,
        HEAL_MP = 3,
        PHYS_DAMAGE = 4,
        FIRE_DAMAGE = 5,
        WIND_DAMAGE = 6,
        WATER_DAMAGE = 7,
        EARTH_DAMAGE = 8,
        ATK_UP = 9,
        ATK_DOWN = 10,
        DEF_UP = 11,
        DEF_DOWN = 12,
        ACT_UP = 13,
        ACT_DOWN = 14,
        POISON_ACTIVE = 15,
        POISON_DEACTIVE = 16,
        PARALYZE_ACTIVE = 17,
        PARALYZE_DEACTIVE = 18,
        MAGICBLOCK_ACTIVE = 19,
        MAGICBLOCK_DEACTIVE = 20
    };

    /**
     * @brief The Geartype enum
     * the type of the gear
     */
    enum Geartype {
        NO_GEAR = 0,
        HEAD = 1,
        CHEST = 2,
        FOOT = 3,
        ACCESSOIRE = 4,
        MANAEGG = 5,
        WEAPON = 6
    };

    /**
     * @brief The Immunity enum
     * immunitylevel
     */
    enum Immunity {
        WEAK = 0,
        NORMAL = 1,
        RESISTENCE = 2,
        IMMUNE = 3
    };

    /**
     * @brief The Ailment enum
     * status ailments
     */
    enum Ailment {
        POISON = 0,
        PARALYZE = 1,
        MAGIC_BLOCK = 2
    };

    /**
     * @brief The Magictype enum
     * the magictype for a spell
     */
    enum Magictype {
        NO_ELEM = 0,
        PHYS = 1,
        FIRE = 2,
        WIND = 3,
        WATER = 4,
        EARTH = 5,
        BOOST = 6,
        AILMENT = 7
    };
}

#endif // VALUES_H
