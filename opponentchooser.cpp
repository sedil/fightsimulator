#include "opponentchooser.h"
#include "fightwindow.h"
#include <QBoxLayout>
#include <QLabel>

namespace fightsim {

OpponentChooser::OpponentChooser(QList<Player*> *players, QList<NPC*> *opponents, QWidget *parent)
    : QWidget{parent}, _players(players), _npcs(opponents) {
    initComponents();
}

OpponentChooser::~OpponentChooser(){}

void OpponentChooser::initComponents(){
    _opponents = new QList<NPC*>();
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout *left = new QBoxLayout(QBoxLayout::TopToBottom);
    _availEnemies = new QListWidget();
    left->addWidget(new QLabel("Available Enemies"));
    left->addWidget(_availEnemies);

    QBoxLayout *middle = new QBoxLayout(QBoxLayout::TopToBottom);
    _addEnemy = new QPushButton(">");
    _removeEnemy = new QPushButton("<");
    middle->addWidget(_addEnemy);
    middle->addWidget(_removeEnemy);

    QBoxLayout *right = new QBoxLayout(QBoxLayout::TopToBottom);
    _choosedEnemies = new QListWidget();
    _confirm = new QPushButton("OK");
    _confirm->setEnabled(false);
    right->addWidget(new QLabel("Choosed Enemies"));
    right->addWidget(_choosedEnemies);
    right->addWidget(_confirm);

    layout->addLayout(left);
    layout->addLayout(middle);
    layout->addLayout(right);

    for(NPC *npc : *_npcs){
        _availEnemies->addItem(npc->getName());
    }

    _availEnemies->setCurrentRow(0);
    _removeEnemy->setEnabled(false);

    setLayout(layout);
    setWindowTitle("Choose Opponents");
    setWindowModality(Qt::WindowModality::ApplicationModal);

    connect(_addEnemy,&QPushButton::clicked, this, [this]{addEnemyToList();});
    connect(_removeEnemy,&QPushButton::clicked, this, [this]{removeEnemyFromList();});
    connect(_confirm,&QPushButton::clicked, this, [this]{confirm();});

    show();
}

// SLOTS //
void OpponentChooser::addEnemyToList(){
    const QString enemy = _availEnemies->takeItem(_availEnemies->currentRow())->text();
    _choosedEnemies->addItem(enemy);
    _removeEnemy->setEnabled(true);
    if(_availEnemies->count() == 0){
        _addEnemy->setEnabled(false);
    }
    if(_choosedEnemies->count() > 0){
        _confirm->setEnabled(true);
        _choosedEnemies->setCurrentRow(0);
    } else {
        _confirm->setEnabled(false);
    }
}

void OpponentChooser::removeEnemyFromList(){
    const QString enemy = _choosedEnemies->takeItem(_choosedEnemies->currentRow())->text();
    _availEnemies->addItem(enemy);
    _availEnemies->setCurrentRow(0);
    _addEnemy->setEnabled(true);

    if(_choosedEnemies->count() == 0){
        _removeEnemy->setEnabled(false);
        _confirm->setEnabled(false);
    }
}

void OpponentChooser::confirm(){
    ushort amount = _choosedEnemies->count();
    for(auto n = 0; n < amount; n++){
        const QString enemyname = _choosedEnemies->takeItem(0)->text();
        for(NPC *npc : *_npcs){
            if(enemyname == npc->getName()){
                _opponents->append(npc);
            }
        }
    }

    close();
    new Fightwindow(_players,_opponents);
}

}
