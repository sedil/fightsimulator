#ifndef MANAEGG_H
#define MANAEGG_H

#include "itemobject.h"

namespace fightsim {

    /**
     * @brief The ManaEgg class subclass of itemobject
     */
    class ManaEgg : public ItemObject {
    private:
        /* values between 0 and 3. 0 means no booster, no additional damage */
        ushort _fireboost;
        ushort _windboost;
        ushort _waterboost;
        ushort _earthboost;
    public:
        explicit ManaEgg();
        explicit ManaEgg(QString name, QString desc, ushort fireboost, ushort windboost, ushort waterboost, ushort earthboost, ushort value);
        explicit ManaEgg(const ManaEgg &copy);
        virtual ~ManaEgg();

        ushort getFireboostLevel() const;
        ushort getWindboostLevel() const;
        ushort getWaterboostLevel() const;
        ushort getEarthboostLevel() const;
    };

}

#endif // MANAEGG_H
