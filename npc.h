#ifndef NPC_H
#define NPC_H

#include "actor.h"
#include "reward.h"

namespace fightsim {

    /**
     * @brief The NPC class
     * subclass of actor. NPCs are enemies and bosses
     */
    class NPC : public Actor {
    private:
        /* only enemies or bosses can give rewards */
        Reward _reward;
    public:
        explicit NPC(QString name, ushort level, ushort hp, ushort mp, ushort str, ushort vit, ushort agi, short phys, short fire, short wind, short water, short earth, Reward reward);
        virtual ~NPC();
        Reward getReward() const;
    };
}

#endif // NPC_H
