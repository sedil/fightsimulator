#include "consum.h"

namespace fightsim {

    ConsumItem::ConsumItem() :
        ItemObject("","",Itemtype::NO_ITEM,0,QPair<Effect,ushort>(Effect::NO_EFFECT,0)){
    }

    ConsumItem::ConsumItem(QString name, QString desc, ushort value, QPair<Effect, short> attribute) :
        ItemObject(name,desc,Itemtype::CONSUMABLE,value,attribute) { }

    ConsumItem::ConsumItem(const ConsumItem &copy) :
        ItemObject(copy.getItemname(),copy.getItemdescription(),copy.getType(),copy.getValue(),copy.getAttribute()) { }

    ConsumItem::~ConsumItem() { }

}
