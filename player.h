#ifndef PLAYER_H
#define PLAYER_H

#include "actor.h"
#include "gearslot.h"
#include "inventar.h"

namespace fightsim {

    class GearSlot;

    /**
     * @brief The Player class
     * subclass of actor
     */
    class Player : public Actor {
    private:
        /* actual experience points */
        ushort _actXp;
        /* experience points to levelup */
        ushort _maxXp;
        GearSlot* _equipment;
    public:
        inline static QMap<ushort,uint> xpMap;
        inline static ushort gold;
        inline static Inventar *inventar;

        explicit Player(QString name, ushort level, ushort hp, ushort mp, ushort str, ushort vit, ushort agi, short phys, short fire, short wind, short water, short earth, ushort xp, ushort maxXP);
        virtual ~Player();
        void initInventar(Inventar *inventar);
        void initGold(ushort gold);
        void addXp(ushort xp);
        void levelUp();
        void changeStrength(short str);
        void changeVitality(short vit);
        void changeAgility(short agi);
        void addGold(short change);
        ushort getActualXp() const;
        ushort getLevelXp() const;
        ushort getGoldAmount() const;

        GearSlot* accessGearSlot() const;
        Inventar* accessInventar();
        void printInfo();
    };
}

#endif // PLAYER_H
