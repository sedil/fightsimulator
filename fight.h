#ifndef FIGHT_H
#define FIGHT_H

#include <QObject>
#include "fightwindow.h"
#include "player.h"
#include "npc.h"
#include "consum.h"

namespace fightsim {

class Fightwindow;

    /**
     * @brief The Fight class
     * This class provides calculations and the logic part of the game
     */
    class Fight {
    private:
        Fightwindow *_fightwindow;
        QList<Player*> _players;
        QList<NPC*> _enemies;
        bool _isPlayerTurn;

        /* index of the active player or npc */
        ushort _actualActorIndex;
        Actor *_actualActor;

        /* index to update a specific actorpanel */
        uint _guiIndex;
    public:
        explicit Fight(Fightwindow *fightframe, QList<Player*> &players, QList<NPC*> &enemies);
        virtual ~Fight();

        void turnloop();
        QPair<bool,bool> isFightOver();
        void menu(Actor *active);
        void changeTurn();
        void changeActor();
        void lootRewards();

        ushort chooseTarget(bool self) const;
        MagicItem* chooseSpell(Actor *active);
        ConsumItem* chooseItem();
        void doPhysicAttack(Actor *active, Actor *target);
        void doMagicAttack(Actor *active, MagicItem *spell, ushort index);

        float examineAttackBoostLevel(Actor *active);
        float examineDefendBoostLevel(Actor *active);
        float examineResistence(MagicItem *spell, Actor *target);
        float examineResistence(ConsumItem *item, Actor *target);
        float examineResistence(Actor *target);
        void doDamage(Actor *target, short damage);

        void ailment(MagicItem *spell, ushort targetindex);
        void poisonEffect(Actor *actor);
        void boost(MagicItem *spell);
        void heal(Actor *active, MagicItem *spell, QList<Actor*> target, ushort index);
        void revive(Actor *active, MagicItem *spell, Actor *target);
        void useItem(ConsumItem *item, Actor *target);

        void setIndexForGUI(uint index);
        Actor* getActiveActor();
    };

}

#endif // FIGHT_H
