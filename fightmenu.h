#ifndef FIGHTMENU_H
#define FIGHTMENU_H

#include <QObject>
#include <QLabel>
#include <QPushButton>
#include <QListWidget>
#include "actor.h"
#include "fight.h"

namespace fightsim {

/**
 * @brief The Fightmenu class
 * Helperclass to choose between combo, magic and items. Also for choose the target
 */
class Fightmenu : public QWidget {
private:
    Q_OBJECT
    Fight *_fight;
    Actor *_active;
    QList<Actor*> *_targets;
    QLabel *_infolabel;
    QPushButton *_combo;
    QPushButton *_magic;
    QPushButton *_item;

    QLabel *_objectdescription;
    QListWidget *_targetlist;
    QListWidget *_objectlist;
    QPushButton *_confirm;

    bool _physAttack;
    bool _magicAttack;
    bool _itemUsage;

    void initComponents();
public:
    explicit Fightmenu(Fight *fight, Actor *actor, QList<Actor*> *opponents, QWidget *parent = nullptr);
    virtual ~Fightmenu();

public slots:
    void updateObjectdescription();
    void combo();
    void magic();
    void item();
    void confirm();

};

}

#endif // FIGHTMENU_H
