#include "inventar.h"

namespace fightsim {

    Inventar::Inventar() { }

    Inventar::~Inventar() {
        for(auto n = 0; n < _itemlist.size(); n++){
            delete _itemlist.at(n);
        }
    }

    void Inventar::addItem(ItemObject *item){
        if(item != nullptr){
            _itemlist.append(item);
        }
    }

    void Inventar::deleteItem(QString itemname){
        uint index = -1;
        for(auto n = 0; n < _itemlist.size(); n++){
            if(_itemlist[n]->getItemname() == itemname){
                index = n;
                break;
            }
        }
        _itemlist.removeAt(index);
    }

    ItemObject* Inventar::getItemCopy(uint index){
        if(index < _itemlist.size()){
            ItemObject *tmp = _itemlist.at(index);
            return tmp;
        }
        return nullptr;
    }

    ItemObject* Inventar::getItem(uint index){
        if(index < _itemlist.size()){
            ItemObject *tmp = _itemlist[index];
            _itemlist.removeAt(index);
            return tmp;
        }
        return nullptr;
    }

    QList<ItemObject*>* Inventar::filterItems(Itemtype itemtype){
        QList<ItemObject*> *filterresult = new QList<ItemObject*>();
        for(auto item : _itemlist){
            if(item->getType() == itemtype){
                filterresult->append(item);
            }
        }
        return filterresult;
    }

    QList<ItemObject*>* Inventar::getInventar() {
        return &_itemlist;
    }

}
