#include "equipwindow.h"

#include <QBoxLayout>

namespace fightsim {

EquipWindow::EquipWindow(Player *player, QWidget *parent)
    : QWidget{parent}, _player(player){
    initComponents();
    fillCombobox();
    updateMagicspelllist();
    setWindowTitle("Gear and Magic Equipment");
    setMinimumSize(640,360);
    setWindowModality(Qt::WindowModality::ApplicationModal);
    show();
}

EquipWindow::~EquipWindow() { }

void EquipWindow::fillCombobox(){
    _head->addItem(_player->accessGearSlot()->showHeadgear()->getItemname());
    _chest->addItem(_player->accessGearSlot()->showChestgear()->getItemname());
    _foot->addItem(_player->accessGearSlot()->showFootgear()->getItemname());
    _accessoire->addItem(_player->accessGearSlot()->showAccessoire()->getItemname());
    _weapon->addItem(_player->accessGearSlot()->showWeapon()->getItemname());
    _manaegg->addItem(_player->accessGearSlot()->showManaegg()->getItemname());

    for(ItemObject *item : *Player::inventar->getInventar()){
        if(item->getType() == Itemtype::GEAR){
            GearItem *tmp = dynamic_cast<GearItem*>(item);
            switch(tmp->getGearType()){
            case Geartype::HEAD: {
                if(!(_head->itemText(0) == tmp->getItemname()))
                _head->addItem(tmp->getItemname());
                break;
            }
            case Geartype::CHEST: {
                if(!(_chest->itemText(0) == tmp->getItemname()))
                _chest->addItem(tmp->getItemname());
                break;
            }
            case Geartype::FOOT: {
                if(!(_foot->itemText(0) == tmp->getItemname()))
                _foot->addItem(tmp->getItemname());
                break;
            }
            case Geartype::ACCESSOIRE: {
                if(!(_accessoire->itemText(0) == tmp->getItemname()))
                _accessoire->addItem(tmp->getItemname());
                break;
            }
            case Geartype::WEAPON: {
                if(!(_weapon->itemText(0) == tmp->getItemname()))
                _weapon->addItem(tmp->getItemname());
                break;
            }
            case Geartype::MANAEGG: {
                if(!(_manaegg->itemText(0) == tmp->getItemname()))
                _manaegg->addItem(tmp->getItemname());
                break;
            }
            default:
                break;
            }
        } else if (item->getType() == Itemtype::EGG){
            ManaEgg *tmp = dynamic_cast<ManaEgg*>(item);
            if(!(_manaegg->itemText(0) == tmp->getItemname())){
                _manaegg->addItem(tmp->getItemname());
            }
        }
    }
}

void EquipWindow::updateMagicspelllist(){
    QList<MagicItem*> spells = _player->accessMagicSlot()->getSpelllist();
    for(const MagicItem *spell : spells){
        _activeSpells->addItem(spell->getItemname());
    }
    for(const ItemObject *spell : *Player::inventar->getInventar()){
        if(spell->getType() == Itemtype::MAGICSPELL){
            _availableSpells->addItem(spell->getItemname());
        }
    }

    if(_activeSpells->count() == 0){
        _removeSpell->setEnabled(false);
    } else {
        _activeSpells->setCurrentRow(0);
    }
    if(_availableSpells->count() == 0){
        _addSpell->setEnabled(false);
    } else {
        _availableSpells->setCurrentRow(0);
    }
}

void EquipWindow::initComponents(){
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout *equiplayout = new QBoxLayout(QBoxLayout::TopToBottom);
    QGridLayout *grid = new QGridLayout();
    _head = new QComboBox();
    _chest = new QComboBox();
    _foot = new QComboBox();
    _accessoire = new QComboBox();
    _weapon = new QComboBox();
    _manaegg = new QComboBox();

    _headlabel = new QLabel("Head ");
    _chestlabel = new QLabel("Chest ");
    _footlabel = new QLabel("Foot ");
    _accessoirelabel = new QLabel("Accessoire ");
    _weaponlabel = new QLabel("Weapon ");
    _manaegglabel = new QLabel("Manaegg ");

    grid->addWidget(_headlabel, 0, 0);
    grid->addWidget(_head, 0, 1);
    grid->addWidget(_chestlabel, 1, 0);
    grid->addWidget(_chest, 1, 1);
    grid->addWidget(_footlabel, 2, 0);
    grid->addWidget(_foot, 2, 1);
    grid->addWidget(_accessoirelabel, 3, 0);
    grid->addWidget(_accessoire, 3, 1);
    grid->addWidget(_weaponlabel, 4, 0);
    grid->addWidget(_weapon, 4, 1);
    grid->addWidget(_manaegglabel, 5, 0);
    grid->addWidget(_manaegg, 5, 1);

    _activeSpells = new QListWidget();
    _removeSpell = new QPushButton("Remove");

    _availableSpells = new QListWidget();
    _addSpell = new QPushButton("Add");

    QBoxLayout *magiclayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout *active = new QBoxLayout(QBoxLayout::TopToBottom);
    active->addWidget(_activeSpells);
    active->addWidget(_removeSpell);

    QBoxLayout *available = new QBoxLayout(QBoxLayout::TopToBottom);
    available->addWidget(_availableSpells);
    available->addWidget(_addSpell);

    magiclayout->addLayout(active);
    magiclayout->addLayout(available);

    connect(_head,&QComboBox::currentIndexChanged, this, [this] {equipHeadgear();});
    connect(_chest,&QComboBox::currentIndexChanged, this, [this] {equipChestgear();});
    connect(_foot,&QComboBox::currentIndexChanged, this, [this] {equipFootgear();});
    connect(_weapon,&QComboBox::currentIndexChanged, this, [this] {equipWeapon();});
    connect(_accessoire,&QComboBox::currentIndexChanged, this, [this] {equipAccessoire();});
    connect(_manaegg,&QComboBox::currentIndexChanged, this, [this] {equipManaegg();});
    connect(_removeSpell, &QPushButton::clicked, this, [this] {removeSpellFromList();});
    connect(_addSpell, &QPushButton::clicked, this, [this]{addSpellToList();});

    equiplayout->addLayout(grid);
    layout->addLayout(equiplayout);
    layout->addLayout(magiclayout);
    setLayout(layout);
}

void EquipWindow::equipHeadgear(){
    QString itemname = _head->currentText();
    for(ItemObject *item : *Player::inventar->getInventar()){
        if(itemname == item->getItemname()){
            // equip item what was choosed
            GearItem *oldGear = _player->accessGearSlot()->equipHeadgear(dynamic_cast<GearItem*>(item));
            // add unequipped item to the player's inventar
            Player::inventar->addItem(oldGear);
            // delete item from the inventar what is now equipped
            Player::inventar->deleteItem(itemname);
            break;
        }
    }
}

void EquipWindow::equipChestgear(){
    QString itemname = _chest->currentText();
    for(ItemObject *item : *Player::inventar->getInventar()){
        if(itemname == item->getItemname()){
            GearItem *oldGear = _player->accessGearSlot()->equipChestgear(dynamic_cast<GearItem*>(item));
            Player::inventar->addItem(oldGear);
            Player::inventar->deleteItem(itemname);
            break;
        }
    }
}

void EquipWindow::equipFootgear(){
    QString itemname = _foot->currentText();
    for(ItemObject *item : *Player::inventar->getInventar()){
        if(itemname == item->getItemname()){
            GearItem *oldGear = _player->accessGearSlot()->equipFootgear(dynamic_cast<GearItem*>(item));
            Player::inventar->addItem(oldGear);
            Player::inventar->deleteItem(itemname);
            break;
        }
    }
}

void EquipWindow::equipAccessoire(){
    QString itemname = _accessoire->currentText();
    for(ItemObject *item : *Player::inventar->getInventar()){
        if(itemname == item->getItemname()){
            GearItem *oldGear = _player->accessGearSlot()->equipAccessoire(dynamic_cast<GearItem*>(item));
            Player::inventar->addItem(oldGear);
            Player::inventar->deleteItem(itemname);
            break;
        }
    }
}

void EquipWindow::equipWeapon(){
    QString itemname = _weapon->currentText();
    for(ItemObject *item : *Player::inventar->getInventar()){
        if(itemname == item->getItemname()){
            GearItem *oldWeapon = _player->accessGearSlot()->equipWeapon(dynamic_cast<GearItem*>(item));
            Player::inventar->addItem(oldWeapon);
            Player::inventar->deleteItem(itemname);
            break;
        }
    }
}

void EquipWindow::equipManaegg(){
    QString itemname = _manaegg->currentText();
    for(ItemObject *item : *Player::inventar->getInventar()){
        if(itemname == item->getItemname()){
            ManaEgg *oldGear = _player->accessGearSlot()->equipManaegg(dynamic_cast<ManaEgg*>(item));
            Player::inventar->addItem(oldGear);
            Player::inventar->deleteItem(itemname);
            break;
        }
    }
}

void EquipWindow::removeSpellFromList(){
    const QString magic = _activeSpells->takeItem(_activeSpells->currentRow())->text();
    for(auto n = 0; n < _player->accessMagicSlot()->getSpelllist().size(); n++){
        if(magic == _player->accessMagicSlot()->getSpell(n)->getItemname()){
            MagicItem *spell = _player->accessMagicSlot()->removeSpell(n);
            _availableSpells->addItem(magic);
            Player::inventar->addItem(spell);
            break;
        }
    }
    _addSpell->setEnabled(true);
    _availableSpells->setCurrentRow(0);

    if(_activeSpells->count() == 0){
        _removeSpell->setEnabled(false);
    }
}

void EquipWindow::addSpellToList(){
    const QString magic = _availableSpells->takeItem(_availableSpells->currentRow())->text();
    for(auto n = 0; n < Player::inventar->getInventar()->size(); n++){
        if(Player::inventar->getInventar()->at(n)->getItemname() == magic){
            MagicItem *spell = new MagicItem(*dynamic_cast<MagicItem*>(Player::inventar->getInventar()->at(n)));
            _player->accessMagicSlot()->addSpellToList(spell);
            _activeSpells->addItem(magic);
            delete Player::inventar->getItem(n);
            break;
        }
    }
    _removeSpell->setEnabled(true);
    _activeSpells->setCurrentRow(0);

    if(_availableSpells->count() == 0){
        _addSpell->setEnabled(false);
    }
}

}
