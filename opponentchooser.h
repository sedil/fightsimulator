#ifndef OPPONENTCHOOSER_H
#define OPPONENTCHOOSER_H

#include <QWidget>
#include <QPushButton>
#include <QListWidget>
#include "player.h"
#include "npc.h"

namespace fightsim {

/**
 * @brief The OpponentChooser class
 * A class for choosing enemies for the next fight
 */
class OpponentChooser : public QWidget {
private:
    Q_OBJECT
    QList<Player*> *_players;
    QList<NPC*> *_npcs;
    QList<NPC*> *_opponents;
    QListWidget *_availEnemies;
    QListWidget *_choosedEnemies;
    QPushButton *_addEnemy;
    QPushButton *_removeEnemy;
    QPushButton *_confirm;
    void initComponents();
public:
    explicit OpponentChooser(QList<Player*> *players, QList<NPC*> *opponents, QWidget *parent = nullptr);
    virtual ~OpponentChooser();

public slots:
    void addEnemyToList();
    void removeEnemyFromList();
    void confirm();

};

}

#endif // OPPONENTCHOOSER_H
