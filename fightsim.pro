QT       += core gui

greaterThan(QT_MAJOR_VERSION, 5): QT += widgets

CONFIG += c++20

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    actor.cpp \
    consum.cpp \
    datahandler.cpp \
    equipwindow.cpp \
    fight.cpp \
    fightmenu.cpp \
    fightwindow.cpp \
    gear.cpp \
    gearslot.cpp \
    inventar.cpp \
    itemobject.cpp \
    itemshop.cpp \
    itemshopgui.cpp \
    magic.cpp \
    magicslot.cpp \
    main.cpp \
    mainmenu.cpp \
    manaegg.cpp \
    npc.cpp \
    opponentchooser.cpp \
    player.cpp

HEADERS += \
    actor.h \
    consum.h \
    datahandler.h \
    equipwindow.h \
    fight.h \
    fightmenu.h \
    fightwindow.h \
    gear.h \
    gearslot.h \
    inventar.h \
    itemobject.h \
    itemshop.h \
    itemshopgui.h \
    magic.h \
    magicslot.h \
    mainmenu.h \
    manaegg.h \
    npc.h \
    opponentchooser.h \
    player.h \
    reward.h \
    values.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES +=
