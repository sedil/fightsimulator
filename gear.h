#ifndef GEAR_H
#define GEAR_H

#include "itemobject.h"

namespace fightsim {

    /**
     * @brief The GearItem class A subclass of itemobject
     */
    class GearItem : public ItemObject {
    private:
        /* kind of gear: HEAD, CHEST, ... */
        Geartype _geartype;
    public:
        explicit GearItem();
        explicit GearItem(QString name, QString desc,Geartype gear, ushort value, QPair<Effect, short> attribute);
        explicit GearItem(const GearItem &copy);
        virtual ~GearItem();

        Geartype getGearType() const;
    };

}

#endif // GEAR_H
