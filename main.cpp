#include "mainmenu.h"

#include <QApplication>

using namespace fightsim;

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    QString path = argv[1];
    MainMenu w(path);
    return a.exec();

    return 0;
}
