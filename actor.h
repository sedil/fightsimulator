#ifndef ACTOR_H
#define ACTOR_H

#include <QObject>
#include <QMap>
#include "magicslot.h"

namespace fightsim {

    /**
     * @brief The Actor class Baseclass of Player and NPC
     */
    class Actor {
    private:

    protected:
        QString _name;
        ushort _level;
        ushort _actHP;
        ushort _maxHP;
        ushort _actMP;
        ushort _maxMP;
        ushort _strength;
        ushort _vitality;
        ushort _agility;

        /* resistences */
        short _physRes;
        short _fireRes;
        short _windRes;
        short _waterRes;
        short _earthRes;

        /* attributes like Attackpower during the fight */
        QMap<QString, short> _fightAttributes;

        /* contains status ailment flags */
        QMap<Ailment, bool> _statusAilment;

        /* counters how long this effect takes place */
        ushort _paralyzeeffectCounter;
        ushort _magicblockCounter;

        /* actor is either a player or an opponent (NPC)*/
        bool _isOpponent;
        bool _isAlive;

        /* contains all available magicspells during fight */
        MagicSlot* _spells;
    public:
        explicit Actor(QString name, ushort level, ushort hp, ushort mp, ushort str, ushort vit, ushort agi, short phys, short fire, short wind, short water, short earth, bool isOpponent);
        virtual ~Actor();
        void changeHP(short hp);
        void changeMP(short mp);
        void setAliveFlag(bool alive);
        void changeAttribute(QString key, short value);
        void changeAilmentFlag(Ailment ailment, bool flag);
        QString getName() const;
        ushort getLevel() const;
        ushort getStrength() const;
        ushort getVitality() const;
        ushort getAgility() const;
        ushort getActualHP() const;
        ushort getMaximumHP() const;
        ushort getActualMP() const;
        ushort getMaximumMP() const;
        bool isOpponent() const;
        bool isAlive() const;
        short getPhysResistance() const;
        short getFireResistance() const;
        short getWindResistance() const;
        short getWaterResistance() const;
        short getEarthResistance() const;
        void paralyzeeffectCounter();
        void magicblockCounter();
        short getAttribute(QString key) const;
        bool getAilmentFlag(Ailment ailment) const;
        MagicSlot* accessMagicSlot() const;
    };
}
#endif // ACTOR_H
