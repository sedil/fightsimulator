#include "fight.h"
#include <QTextStream>

namespace fightsim {

    Fight::Fight(Fightwindow *fightframe, QList<Player*> &players, QList<NPC*> &enemies) :
        _fightwindow(fightframe), _players(players), _enemies(enemies) {
        ushort playersAgi = 0;
        ushort enemiesAgi = 0;
        _actualActorIndex = 0;
        _guiIndex = 0;
        for(auto *player : _players){
            playersAgi += player->getAgility();
        }
        for(auto *enemy : _enemies){
            enemiesAgi += enemy->getAgility();
        }
        if(playersAgi >= enemiesAgi){
            _isPlayerTurn = true;
            _actualActor = _players.at(_actualActorIndex);
            _fightwindow->updateFightinfo("Player starts");
            _fightwindow->updateFightinfo(_actualActor->getName()+"'s Turn");
        } else {
            _isPlayerTurn = false;
            _actualActor = _enemies.at(_actualActorIndex);
            _fightwindow->updateFightinfo("Enemy starts");
            _fightwindow->updateFightinfo(_actualActor->getName()+"'s Turn");
        }
    }

    Fight::~Fight(){
        for(Actor *enemy : _enemies){
            delete enemy;
        }
    }

    /**
     * @brief Fight::turnloop
     * this function controls one actors action
     */
    void Fight::turnloop(){
        if(_isPlayerTurn){

            /* check for paralyze ailment and if the player at index lives */
            for(auto n = _actualActorIndex; n < _players.size(); n++){
                if(_players.at(n)->isAlive() && !_players.at(n)->getAilmentFlag(Ailment::PARALYZE)){
                    break;
                } else if(_players.at(n)->isAlive() && _players.at(n)->getAilmentFlag(Ailment::PARALYZE)){
                    _players.at(n)->paralyzeeffectCounter();
                    _fightwindow->updateFightinfo(_players.at(n)->getName() + " is Paralyzed");
                }
            }

            /* choose active player for this turn */
            _actualActor = _players.at(_actualActorIndex);

            /* make poison damage if poison ailment is active */
            if(_actualActor->getAilmentFlag(Ailment::POISON)){
                poisonEffect(_actualActor);
            }

            /* examine, if the fight is over, if all players are dead */
            auto turnend = isFightOver();
            if(!turnend.second){
                /* choose next player */
                changeActor();
            } else {
                if(turnend.first){
                    _fightwindow->updateFightinfo("Enemy win");
                    _fightwindow->fightEnd();
                    return;
                } else {
                    _fightwindow->updateFightinfo("Player win");
                    lootRewards();
                    _fightwindow->fightEnd();
                    return;
                }
            }
        } else {
            for(auto n = _actualActorIndex; n < _enemies.size(); n++){
                if(_enemies.at(n)->isAlive() && !_enemies.at(n)->getAilmentFlag(Ailment::PARALYZE)){
                    break;
                } else if(_enemies.at(n)->isAlive() && _enemies.at(n)->getAilmentFlag(Ailment::PARALYZE)){
                    _enemies.at(n)->paralyzeeffectCounter();
                    _fightwindow->updateFightinfo(_enemies.at(n)->getName() + " is Paralyzed");
                }
            }
            _actualActor = _enemies.at(_actualActorIndex);

            if(_actualActor->getAilmentFlag(Ailment::POISON)){
                poisonEffect(_actualActor);
            }

            auto turnend = isFightOver();
            if(!turnend.second){
                changeActor();
            } else {
                if(turnend.first){
                    _fightwindow->updateFightinfo("Enemy win");
                    return;
                } else {
                    _fightwindow->updateFightinfo("Player win");
                    lootRewards();
                    return;
                }
            }
        }
    }

    /**
     * @brief Fight::lootRewards
     * After a success fight, players get rewarded
     */
    void Fight::lootRewards(){
        /* loop through all enemies to loot their rewards and put them into players inventar */
        for(auto n = 0; n < _enemies.size(); n++){
            NPC *opponent = dynamic_cast<NPC*>(_enemies.at(n));
            Reward reward = opponent->getReward();
            _fightwindow->updateFightinfo("");
            if(reward.itemReward != nullptr){
                Player::inventar->addItem(reward.itemReward);
                _fightwindow->updateFightinfo("Added " + reward.itemReward->getItemname() + " to the inventar");
            }
            Player::gold += reward.goldReward;
            _fightwindow->updateFightinfo("Added " + QString::number(reward.goldReward) + " GOLD to the inventar");
            _fightwindow->updateFightinfo("Gained " + QString::number(reward.xpReward) + " XP");
            for(auto m = 0; m < _players.size(); m++){
                Player *player = dynamic_cast<Player*>(_players.at(m));
                player->addXp(reward.xpReward);
            }
        }
    }

    /**
     * @brief Fight::isFightOver
     * @return first argument determines if player or enemy wins. Only if second argument is true, the fight is over
     */
    QPair<bool,bool> Fight::isFightOver(){
        ushort playersAlive = _players.size();
        ushort enemiesAlive = _enemies.size();

        for(Actor *enemy : _enemies){
            if(!enemy->isAlive()){
                enemiesAlive--;
            }
        }

        for(Actor *player : _players){
            if(!player->isAlive()){
                playersAlive--;
            }
        }

        if(playersAlive == 0){
            return QPair<bool,bool>(true,true);
        } else if(enemiesAlive == 0){
            return QPair<bool,bool>(false,true);
        } else {
            return QPair<bool,bool>(false,false);
        }
    }

    /**
     * @brief Fight::changeTurn
     * changes the turn from playergroup to enemygroup and vice versa
     */
    void Fight::changeTurn(){
        if(_isPlayerTurn){
            _isPlayerTurn = false;
            _fightwindow->updateFightinfo("Enemy Turn");
            _actualActorIndex = 0;
            _actualActor = _enemies.at(_actualActorIndex);
        } else {
            _isPlayerTurn = true;
            _fightwindow->updateFightinfo("Player Turn");
            _actualActorIndex = 0;
            _actualActor = _players.at(_actualActorIndex);
        }
    }

    /**
     * @brief Fight::changeActor
     * changes the actor within player- or enemygroup
     */
    void Fight::changeActor(){
        _actualActorIndex += 1;
        if(_isPlayerTurn){
            if(_actualActorIndex == _players.size()){
                changeTurn();
                return;
            }
            for(auto n = _actualActorIndex; n < _players.size(); n++){
                if(!_players.at(_actualActorIndex)->isAlive()){
                    continue;
                }
                _actualActor = _players.at(_actualActorIndex);
                _fightwindow->updateFightinfo("");
                _fightwindow->updateFightinfo(_actualActor->getName());
                return;
            }
        } else {
            if(_actualActorIndex == _enemies.size()){
                changeTurn();
                return;
            }
            for(auto n = _actualActorIndex; n < _enemies.size(); n++){
                if(!_enemies.at(_actualActorIndex)->isAlive()){
                    continue;
                }
                _actualActor = _enemies.at(_actualActorIndex);
                _fightwindow->updateFightinfo("");
                _fightwindow->updateFightinfo(_actualActor->getName());
                return;
            }
        }
    }

    /**
     * @brief Fight::doPhysicAttack commits a physical attack to the choosen target
     * @param active the active player or npc
     * @param target the choosed target
     */
    void Fight::doPhysicAttack(Actor *active, Actor *target){
        short damage = static_cast<short>(active->getStrength() * 3 * examineAttackBoostLevel(active));
        short reduce = static_cast<short>(target->getVitality() * 2 * examineResistence(target) * examineDefendBoostLevel(target));
        short total = damage - reduce;
        if(total <= 0){
            doDamage(target, 0);
        } else {
            doDamage(target, -total);
        }
    }

    /**
     * @brief Fight::doMagicAttack
     * @param active who casted the spell
     * @param spell which spell is casted
     * @param index which targetindex receives damage. For AOE index is irrelevant.
     */
    void Fight::doMagicAttack(Actor *active, MagicItem *spell, ushort index){
        /* enough MP? */
        if(active->getActualMP() < spell->getMPUsage()){
            return;
        }
        active->changeMP(-spell->getMPUsage());

        /* In case if this spell is a BOOST oder AILMENT spell */
        if(spell->getMagicType() == Magictype::BOOST){
            boost(spell);
        } else if(spell->getMagicType() == Magictype::AILMENT){
            ailment(spell,index);
        }

        /* Are all actors effected? */
        if(spell->isAOE()){
            if(_isPlayerTurn){
                for(Actor *enemy : _enemies){
                    short damage = static_cast<short>(active->getStrength() * 2 + spell->getBasedamage() * 3 * examineAttackBoostLevel(active));
                    short reduce = static_cast<short>(enemy->getVitality() * 2 * examineResistence(spell,enemy) * examineDefendBoostLevel(enemy));
                    short total = damage - reduce;
                    if(total > 0){
                        doDamage(enemy, -total);
                    }
                }
            } else {
                for(Actor *enemy : _players){
                    short damage = static_cast<short>(active->getStrength() * 2 + spell->getBasedamage() * 3 * examineAttackBoostLevel(active));
                    short reduce = static_cast<short>(enemy->getVitality() * 2 * examineResistence(spell,enemy) * examineDefendBoostLevel(enemy));
                    short total = damage - reduce;
                    if(total > 0){
                        doDamage(enemy, -total);
                    }
                }
            }
        } else {
            if(_isPlayerTurn){
                short damage = static_cast<short>(active->getStrength() * 2 + spell->getBasedamage() * 3 * examineAttackBoostLevel(_enemies.at(index)));
                short reduce = static_cast<short>(_enemies.at(index)->getVitality() * 2 * examineResistence(spell,_enemies.at(index)) * examineDefendBoostLevel(_enemies.at(index)));
                short total = damage - reduce;
                if(total > 0){
                    doDamage(_enemies.at(index), -total);
                }
            } else {
                short damage = static_cast<short>(active->getStrength() * 2 + spell->getBasedamage() * 3 * examineAttackBoostLevel(_enemies.at(index)));
                short reduce = static_cast<short>(_players.at(index)->getVitality() * 2 * examineResistence(spell,_players.at(index)) * examineDefendBoostLevel(_players.at(index)));
                short total = damage - reduce;
                if(total > 0){
                    doDamage(_players.at(index), -total);
                }
            }
        }
    }

    /**
     * @brief Fight::examineAttackBoostLevel
     * @param active get the attackattribute of this actor
     * @return multiplication of damage
     */
    float Fight::examineAttackBoostLevel(Actor *active){
        switch(active->getAttribute("ATK")){
        case -1:
            return 0.75;
        case 0:
            return 1;
        case 1:
            return 1.25;
        }
        return 1;
    }

    /**
     * @brief Fight::examineDefendBoostLevel
     * @param active get the defenseattribute of this actor
     * @return damagereduction based on defenseattribute
     */
    float Fight::examineDefendBoostLevel(Actor *active){
        switch(active->getAttribute("DEF")){
        case -1:
            return 0.75;
        case 0:
            return 1;
        case 1:
            return 1.25;
        }
        return 1;
    }

    /**
     * @brief Fight::examineResistence
     * @param spell casted spell
     * @param target target of this spell
     * @return multiplicator based on the Immunity of this target
     */
    float Fight::examineResistence(MagicItem *spell, Actor *target){
        switch(spell->getMagicType()){
        case Magictype::FIRE:
            if(target->getFireResistance() == Immunity::WEAK){
                return 0.5;
            } else if (target->getFireResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getFireResistance() == Immunity::RESISTENCE){
                return 1.5;
            } else if(target->getFireResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        case Magictype::WIND:
            if(target->getWindResistance() == Immunity::WEAK){
                return 0.5;
            } else if(target->getWindResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getWindResistance() == Immunity::RESISTENCE){
                return 1.5;
            } else if(target->getWindResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        case Magictype::WATER:
            if(target->getWaterResistance() == Immunity::WEAK){
                return 0.5;
            } else if(target->getWaterResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getWaterResistance() == Immunity::RESISTENCE){
                return 1.5;
            } else if(target->getWaterResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        case Magictype::EARTH:
            if(target->getEarthResistance() == Immunity::WEAK){
                return 0.5;
            } else if(target->getEarthResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getEarthResistance() == Immunity::RESISTENCE){
                return 1.5;
            } else if(target->getEarthResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        default:
            return 1;
        }
        return 1;
    }

    float Fight::examineResistence(Actor *target){
        switch(target->getPhysResistance()){
        case Immunity::WEAK:
            return 0.5;
        case Immunity::NORMAL:
            return 1;
        case Immunity::RESISTENCE:
            return 1.5;
        case Immunity::IMMUNE:
            return 0;
        }
        return 1;
    }

    /**
     * @brief Fight::examineResistence
     * @param item choosed item
     * @param target target
     * @return multiplicator based on the targets immunity
     */
    float Fight::examineResistence(ConsumItem *item, Actor *target){
        switch(item->getAttribute().first){
        case Effect::PHYS_DAMAGE:
            if(target->getPhysResistance() == Immunity::WEAK){
                return 1.5;
            } else if (target->getPhysResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getPhysResistance() == Immunity::RESISTENCE){
                return 0.5;
            } else if(target->getPhysResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        case Effect::FIRE_DAMAGE:
            if(target->getFireResistance() == Immunity::WEAK){
                return 1.5;
            } else if (target->getFireResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getFireResistance() == Immunity::RESISTENCE){
                return 0.5;
            } else if(target->getFireResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        case Effect::WIND_DAMAGE:
            if(target->getWindResistance() == Immunity::WEAK){
                return 1.5;
            } else if(target->getWindResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getWindResistance() == Immunity::RESISTENCE){
                return 0.5;
            } else if(target->getWindResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        case Effect::WATER_DAMAGE:
            if(target->getWaterResistance() == Immunity::WEAK){
                return 1.5;
            } else if(target->getWaterResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getWaterResistance() == Immunity::RESISTENCE){
                return 0.5;
            } else if(target->getWaterResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        case Effect::EARTH_DAMAGE:
            if(target->getEarthResistance() == Immunity::WEAK){
                return 1.5;
            } else if(target->getEarthResistance() == Immunity::NORMAL){
                return 1;
            } else if(target->getEarthResistance() == Immunity::RESISTENCE){
                return 0.5;
            } else if(target->getEarthResistance() == Immunity::IMMUNE){
                return 0;
            }
            break;
        default:
            return 1;
        }
        return 1;
    }

    /**
     * @brief Fight::poisonEffect causes poison to the actor
     * @param actor actor receives poison damage
     */
    void Fight::poisonEffect(Actor *actor){
        short poisondamage = static_cast<short>(actor->getMaximumHP() * 0.05);
        actor->changeHP(-poisondamage);
        _fightwindow->updateFightinfo(actor->getName() + "received " + QString::number(poisondamage) + " Poison damage");
    }

    /**
     * @brief Fight::ailment activates status ailment
     * @param spell spell
     * @param targetindex index of the target
     */
    void Fight::ailment(MagicItem *spell, ushort targetindex){
        if(_isPlayerTurn){
            Actor *target = _enemies.at(targetindex);
            switch(spell->getAttribute().first){
            case Effect::POISON_ACTIVE:
                target->changeAilmentFlag(Ailment::POISON,true);
                break;
            case Effect::PARALYZE_ACTIVE:
                target->changeAilmentFlag(Ailment::PARALYZE,true);
                break;
            case Effect::MAGICBLOCK_ACTIVE:
                target->changeAilmentFlag(Ailment::MAGIC_BLOCK,true);
                break;
            default:
                break;
            }
        } else {
            Actor *target = _players.at(targetindex);
            switch(spell->getAttribute().first){
            case Effect::POISON_ACTIVE:
                target->changeAilmentFlag(Ailment::POISON,true);
                break;
            case Effect::PARALYZE_ACTIVE:
                target->changeAilmentFlag(Ailment::PARALYZE,true);
                break;
            case Effect::MAGICBLOCK_ACTIVE:
                target->changeAilmentFlag(Ailment::MAGIC_BLOCK,true);
                break;
            default:
                break;
            }
        }
    }

    /**
     * @brief Fight::boost increases or decreases the fightattributes like
     * Attackpower, Defensepower
     * @param spell boosterspell
     */
    void Fight::boost(MagicItem *spell){
        if(_isPlayerTurn){
            switch(spell->getAttribute().first){
            case Effect::ATK_UP:
                for(Actor *member : _players){
                    member->changeAttribute("ATK", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Playergroup ATTACK UP");
                break;
            case Effect::ATK_DOWN:
                for(Actor *member : _enemies){
                    member->changeAttribute("ATK", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Enemiegroup ATTACK DOWN");
                break;
            case Effect::DEF_UP:
                for(Actor *member : _players){
                    member->changeAttribute("DEF", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Playergroup DEFENSE UP");
                break;
            case Effect::DEF_DOWN:
                for(Actor *member : _enemies){
                    member->changeAttribute("DEF", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Enemiegroup DEFENSE DOWN");
                break;
            case Effect::ACT_UP:
                for(Actor *member : _players){
                    member->changeAttribute("ACT", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Playergroup ACTION UP");
                break;
            case Effect::ACT_DOWN:
                for(Actor *member : _enemies){
                    member->changeAttribute("ACT", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Enemiegroup ACTION DOWN");
                break;
            default:
                break;
            }
        } else {
            switch(spell->getAttribute().first){
            case Effect::ATK_UP:
                for(Actor *member : _enemies){
                    member->changeAttribute("ATK", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Enemiegroup ATTACK UP");
                break;
            case Effect::ATK_DOWN:
                for(Actor *member : _players){
                    member->changeAttribute("ATK", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Playergroup ATTACK DOWN");
                break;
            case Effect::DEF_UP:
                for(Actor *member : _enemies){
                    member->changeAttribute("DEF", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Enemiegroup DEFENSE UP");
                break;
            case Effect::DEF_DOWN:
                for(Actor *member : _players){
                    member->changeAttribute("DEF", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Playergroup DEFENSE DOWN");
                break;
            case Effect::ACT_UP:
                for(Actor *member : _enemies){
                    member->changeAttribute("ACT", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Enemiegroup ACTION UP");
                break;
            case Effect::ACT_DOWN:
                for(Actor *member : _players){
                    member->changeAttribute("ACT", spell->getAttribute().second);
                }
                _fightwindow->updateFightinfo("Playergroup ACTION DOWN");
                break;
            default:
                break;
            }
        }
    }

    /**
     * @brief Fight::heal heals the target
     * @param active caster of this spell
     * @param spell healspell
     * @param target targets which will be healed
     * @param index for singleheal only
     */
    void Fight::heal(Actor *active, MagicItem *spell, QList<Actor *> target, ushort index){
        if(active->getActualMP() < spell->getMPUsage()){
            return;
        }

        active->changeMP(-spell->getMPUsage());
        if(spell->isAOE()){
            for(Actor *healtarget : target){
                healtarget->changeHP(spell->getBasedamage());
            }
        } else {
            target.at(index)->changeHP(spell->getBasedamage());
        }
    }

    /**
     * @brief Fight::revive revives an actor
     * @param active caster of this spell
     * @param spell revivespell
     * @param target target will be revived
     */
    void Fight::revive(Actor *active, MagicItem *spell, Actor *target){
        if(active->getActualMP() < spell->getMPUsage()){
            return;
        }
        active->changeMP(-spell->getMPUsage());
        target->changeHP(target->getMaximumHP());
        target->setAliveFlag(true);
    }

    /**
     * @brief Fight::doDamage
     * @param target receiver of the damage
     * @param damage damage
     */
    void Fight::doDamage(Actor *target, short damage){
        target->changeHP(damage);
        _fightwindow->updateFightinfo(target->getName() + " received " + QString::number(damage) + " Damage");
        _fightwindow->updateActorpanel(target,_guiIndex);
        turnloop();
    }

    /**
     * @brief Fight::useItem use an item on this target
     * @param item item
     * @param target target
     */
    void Fight::useItem(ConsumItem *item, Actor *target){
        Effect first = item->getAttribute().first;
        ushort fValue = item->getAttribute().second;

        short total = 0;
        switch(first){
        case Effect::PHYS_DAMAGE:
            total = static_cast<short>(examineResistence(item,target) * fValue);
            doDamage(target, -total);
            break;
        case Effect::FIRE_DAMAGE:
            total = static_cast<short>(examineResistence(item,target) * fValue);
            doDamage(target, -total);
            break;
        case Effect::WIND_DAMAGE:
            total = static_cast<short>(examineResistence(item,target) * fValue);
            doDamage(target, -total);
            break;
        case Effect::WATER_DAMAGE:
            total = static_cast<short>(examineResistence(item,target) * fValue);
            doDamage(target, -total);
            break;
        case Effect::EARTH_DAMAGE:
            total = static_cast<short>(examineResistence(item,target) * fValue);
            doDamage(target, -total);
            break;
        case Effect::HEAL_HP:
            doDamage(target, fValue);
            break;
        case Effect::HEAL_MP:
            target->changeMP(fValue);
            break;
        case Effect::REVIVE:
            target->changeHP(target->getMaximumHP());
            break;
        case Effect::POISON_ACTIVE:
            target->changeAilmentFlag(Ailment::POISON,true);
            break;
        case Effect::POISON_DEACTIVE:
            target->changeAilmentFlag(Ailment::POISON,false);
            break;
        case Effect::PARALYZE_ACTIVE:
            target->changeAilmentFlag(Ailment::PARALYZE,true);
            break;
        case Effect::PARALYZE_DEACTIVE:
            target->changeAilmentFlag(Ailment::PARALYZE,false);
            break;
        case Effect::MAGICBLOCK_ACTIVE:
            target->changeAilmentFlag(Ailment::MAGIC_BLOCK,true);
            break;
        case Effect::MAGICBLOCK_DEACTIVE:
            target->changeAilmentFlag(Ailment::MAGIC_BLOCK,false);
            break;
        default:
            return;
        }

        Player::inventar->deleteItem(item->getItemname());
    }

    /**
     * @brief Fight::setIndexForGUI
     * @param index index to update a specific actorpanel
     */
    void Fight::setIndexForGUI(uint index){
        _guiIndex = index;
    }

    Actor* Fight::getActiveActor(){
        return _actualActor;
    }
}
