#ifndef REWARD_H
#define REWARD_H

#include <QObject>
#include "itemobject.h"

namespace fightsim {

    /**
     * @brief The Reward struct
     * For NPCs and Bosses only. Containes item, gold and xp which
     * will be dropped after a success fight
     */
    struct Reward {
        ushort xpReward;
        ushort goldReward;
        ItemObject *itemReward;
        Reward(ushort xp, ushort gold, ItemObject *item){
            xpReward = xp;
            goldReward = gold;
            itemReward = item;
        }
    };
}

#endif // REWARD_H
