#include "mainmenu.h"
#include "itemshopgui.h"
#include "equipwindow.h"
#include "opponentchooser.h"
#include <QPushButton>
#include <QBoxLayout>

namespace fightsim {

    MainMenu::MainMenu(QString &path, QWidget *parent)
        : QWidget(parent){

        initComponents(path);
        initSignalAndSlots();
    }

    MainMenu::~MainMenu(){}

    void MainMenu::initComponents(QString &path){
        // Load data //
        _dh = new DataHandler(path);
        _itemlist = _dh->getItemData();
        _npclist = _dh->getNPCs(_itemlist);
        _playerlist = _dh->getPlayers(_itemlist);

        // GUI components //
        _fight = new QPushButton("Fight");
        _equip = new QPushButton("Equip");
        _itemshop = new QPushButton("Itemshop");
        _save = new QPushButton("Save game");

        QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
        layout->addWidget(_fight);
        layout->addWidget(_equip);
        layout->addWidget(_itemshop);
        layout->addWidget(_save);

        setWindowTitle("Fight Simulator");
        setMinimumSize(320,240);
        setLayout(layout);
        show();
    }

    void MainMenu::initSignalAndSlots(){
        connect(_save,&QPushButton::clicked,this,[this]{save();});
        connect(_itemshop,&QPushButton::clicked,this,[this]{itemshopMenu();});
        connect(_equip,&QPushButton::clicked,this,[this]{equipMenu();});
        connect(_fight,&QPushButton::clicked,this,[this]{fightMenu();});
    }

    // Slots //
    void MainMenu::save(){
        _dh->saveProgress(_playerlist);
    }

    void MainMenu::itemshopMenu(){
        ItemshopGUI *shop = new ItemshopGUI(dynamic_cast<Player*>(_playerlist->at(0)), _itemlist);
    }

    void MainMenu::equipMenu(){
        EquipWindow *equip = new EquipWindow(dynamic_cast<Player*>(_playerlist->at(0)));
    }

    void MainMenu::fightMenu(){
        OpponentChooser *oc = new OpponentChooser(_playerlist,_npclist);
    }
}

