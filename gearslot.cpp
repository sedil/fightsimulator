#include <QPair>
#include "gearslot.h"
#include "values.h"

namespace fightsim {

    GearSlot::GearSlot(Player *player) : _player(player) {}

    GearSlot::~GearSlot(){}

    void GearSlot::calculateAttribute(GearItem &gear, bool unequip){
        Effect effect = gear.getAttribute().first;
        short effectvalue = gear.getAttribute().second;

        if(unequip){
            switch(effect){
            case Effect::ATK_UP:
                _player->changeStrength(-effectvalue);
                break;
            case Effect::DEF_UP:
                _player->changeVitality(-effectvalue);
                break;
            case Effect::ACT_UP:
                _player->changeAgility(-effectvalue);
                break;
            default:
                break;
            }
        } else {
            switch(effect){
            case Effect::ATK_UP:
                _player->changeStrength(effectvalue);
                break;
            case Effect::DEF_UP:
                _player->changeVitality(effectvalue);
                break;
            case Effect::ACT_UP:
                _player->changeAgility(effectvalue);
                break;
            default:
                break;
            }
        }
    }

    /**
     * @brief GearSlot::equipHeadgear equips new headgear and return the old equipped item
     * @param head new item
     * @return old item
     */
    GearItem* GearSlot::equipHeadgear(GearItem* head){
        GearItem* oldHead = unequipHeadgear();
        _head = head;
        calculateAttribute(*oldHead, true);
        calculateAttribute(*head, false);
        return oldHead;
    }

    GearItem* GearSlot::equipChestgear(GearItem* chest){
        GearItem* oldChest = unequipChestgear();
        _chest = chest;
        calculateAttribute(*oldChest, true);
        calculateAttribute(*chest, false);
        return oldChest;
    }

    GearItem* GearSlot::equipFootgear(GearItem* foot){
        GearItem* oldFoot = unequipFootgear();
        _foot = foot;
        calculateAttribute(*oldFoot, true);
        calculateAttribute(*foot, false);
        return oldFoot;
    }

    GearItem* GearSlot::equipAccessoire(GearItem* accessoire){
        GearItem* oldAcc = unequipAccessoire();
        _accessoire = accessoire;
        calculateAttribute(*oldAcc, true);
        calculateAttribute(*accessoire, false);
        return oldAcc;
    }

    ManaEgg* GearSlot::equipManaegg(ManaEgg* manaegg){
        ManaEgg* oldEgg = unequipManaegg();
        _manaegg = manaegg;
        return oldEgg;
    }

    GearItem* GearSlot::equipWeapon(GearItem* weapon) {
        GearItem* oldWeapon = unequipWeapon();
        _weapon = weapon;
        calculateAttribute(*oldWeapon, true);
        calculateAttribute(*weapon, false);
        return oldWeapon;
    }

    GearItem* GearSlot::unequipHeadgear() {
        calculateAttribute(*_head, true);
        return _head;
    }

    GearItem* GearSlot::unequipChestgear() {
        calculateAttribute(*_chest, true);
        return _chest;
    }

    GearItem* GearSlot::unequipFootgear() {
        calculateAttribute(*_foot, true);
        return _foot;
    }

    GearItem* GearSlot::unequipAccessoire() {
        calculateAttribute(*_accessoire, true);
        return _accessoire;
    }

    ManaEgg* GearSlot::unequipManaegg() {
        return _manaegg;
    }

    GearItem* GearSlot::unequipWeapon() {
        calculateAttribute(*_weapon, true);
        return _weapon;
    }

    /**
     * @brief GearSlot::initHeadgear call only once for initialisation
     * @param head headgear
     */
    void GearSlot::initHeadgear(GearItem *head){
        _head = head;
    }

    void GearSlot::initChestgear(GearItem *chest){
        _chest = chest;
    }

    void GearSlot::initFootgear(GearItem *foot){
        _foot = foot;
    }

    void GearSlot::initAccessoire(GearItem *accessoire){
        _accessoire = accessoire;
    }

    void GearSlot::initManaEgg(ManaEgg *manaegg){
        _manaegg = manaegg;
    }

    void GearSlot::initWeapon(GearItem *weapon){
        _weapon = weapon;
    }

    GearItem* GearSlot::showHeadgear() {
        return _head;
    }

    GearItem* GearSlot::showChestgear() {
        return _chest;
    }

    GearItem* GearSlot::showFootgear() {
        return _foot;
    }

    GearItem* GearSlot::showAccessoire() {
        return _accessoire;
    }

    ManaEgg* GearSlot::showManaegg() {
        return _manaegg;
    }

    GearItem* GearSlot::showWeapon() {
        return _weapon;
    }

}
