#ifndef ITEMSHOP_H
#define ITEMSHOP_H

#include <QObject>
#include "player.h"
#include "itemobject.h"

namespace fightsim {

    /**
     * @brief The ItemShop class
     * Contains the logicpart of the itemshop
     */
    class ItemShop {
    private:
        Player *_player;
        QList<ItemObject*> _shopitems;
    public:
        explicit ItemShop(Player *player);
        virtual ~ItemShop();

        void loadShopitems(QList<ItemObject*> &shopitems);
        void listShopitems();
        void listInventarItems();
        void sellItem(uint index);
        void buyItem(uint index);
    };

}

#endif // ITEMSHOP_H
