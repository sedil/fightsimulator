#ifndef DATAHANDLER_H
#define DATAHANDLER_H

#include <QObject>
#include <QList>
#include "values.h"
#include "itemobject.h"
#include "player.h"
#include "npc.h"

namespace fightsim {

    /**
     * @brief The DataHandler class
     * A helperclass to load the configuration and JSON files for this game
     */
    class DataHandler : public QObject{
    private:
        QString _configpath;
        QString _itemdatapath;
        QString _npcdatapath;
        QString _playerdatapath;

        void loadData();
        bool fileIsValid(const QString &file);
        Magictype defineMagictype(QString &magictype);
        Geartype defineGeartype(QString &geartype);
        QPair<Effect,short> createEffectPair(QString effect, short value);
    public:
        explicit DataHandler(const QString &path);
        virtual ~DataHandler();

        void saveProgress(const QList<Player*> *playerdata);
        QList<ItemObject*>* getItemData();
        QList<NPC*>* getNPCs(const QList<ItemObject*> *itemlist);
        QList<Player*>* getPlayers(const QList<ItemObject*> *itemlist);
    };

}

#endif // DATAHANDLER_H
