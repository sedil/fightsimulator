#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include "QPushButton"
#include "datahandler.h"

namespace fightsim {

    /**
     * @brief The MainMenu class
     * Mainwindow of this game
     */
    class MainMenu : public QWidget {
    private:
        Q_OBJECT
        QPushButton *_save;
        QPushButton *_itemshop;
        QPushButton *_equip;
        QPushButton *_fight;
        DataHandler *_dh;
        QList<ItemObject*> *_itemlist;
        QList<NPC*> *_npclist;
        QList<Player*> *_playerlist;
        void initComponents(QString &path);
        void initSignalAndSlots();
    public:
        MainMenu(QString &path, QWidget *parent = nullptr);
        ~MainMenu();

    public slots:
        void fightMenu();
        void equipMenu();
        void itemshopMenu();
        void save();
    };
}
#endif // MAINMENU_H
