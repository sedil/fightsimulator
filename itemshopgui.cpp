#include "itemshopgui.h"
#include "consum.h"
#include "manaegg.h"
#include "magic.h"
#include "gear.h"

#include <QBoxLayout>

namespace fightsim {

ItemshopGUI::ItemshopGUI(Player *player, QList<ItemObject*> *shopitems, QWidget *parent)
    : QWidget{parent}, _player(player), _shopitemlist(shopitems) {
    initComponents();
    updateListitems();
}

ItemshopGUI::~ItemshopGUI(){}

void ItemshopGUI::updateListitems(){
    for(const ItemObject *item : *_player->accessInventar()->getInventar()){
        _inventarView->addItem(item->getItemname() + "|" + QString::number(item->getValue()));
    }

    for(const ItemObject *item : *_shopitemlist){
        _shopitemView->addItem(item->getItemname() + "|" + QString::number(item->getValue()));
    }
    _shopitemView->setCurrentRow(0);
}

void ItemshopGUI::initComponents(){
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    QBoxLayout *labels = new QBoxLayout(QBoxLayout::LeftToRight);
    _moneyLabel = new QLabel("Money: ");
    _money = new QLabel(QString::number(Player::gold));
    _itemdescription = new QLabel();
    labels->addWidget(_moneyLabel);
    labels->addWidget(_money);

    QBoxLayout *listlayout = new QBoxLayout(QBoxLayout::LeftToRight);
    _inventarView = new QListWidget();
    _shopitemView = new QListWidget();
    listlayout->addWidget(_inventarView);
    listlayout->addWidget(_shopitemView);

    QBoxLayout *infolayout = new QBoxLayout(QBoxLayout::TopToBottom);
    infolayout->addWidget(_itemdescription);

    QBoxLayout *buttonlayout = new QBoxLayout(QBoxLayout::LeftToRight);
    _buy = new QPushButton("Buy");
    _sell = new QPushButton("Sell");
    buttonlayout->addWidget(_sell);
    buttonlayout->addWidget(_buy);

    layout->addLayout(labels);
    layout->addLayout(listlayout);
    layout->addLayout(infolayout);
    layout->addLayout(buttonlayout);
    setLayout(layout);

    connect(_inventarView,&QListWidget::currentRowChanged,this,[this]{showItemDescription(true);});
    connect(_shopitemView,&QListWidget::currentRowChanged,this,[this]{showItemDescription(false);});
    connect(_sell,&QPushButton::clicked, this, [this]{sellItem();});
    connect(_buy,&QPushButton::clicked, this, [this]{buyItem();});

    setWindowTitle("Itemshop");
    setMinimumSize(640,360);
    setWindowModality(Qt::WindowModality::ApplicationModal);
    show();
}

// Slots //

void ItemshopGUI::showItemDescription(bool isInventar){
    if(isInventar){
        if(_inventarView->count() <= 1){
            _inventarView->setCurrentRow(0);
        }
        int index = _inventarView->currentRow();
        ItemObject *item = _player->accessInventar()->getItemCopy(index);
        _itemdescription->setText(item->getItemdescription());
    } else {
        int index = _shopitemView->currentRow();
        ItemObject *item = _shopitemlist->at(index);
        _itemdescription->setText(item->getItemdescription());
    }
}

void ItemshopGUI::sellItem(){
    if(_inventarView->count() == 0){
        return;
    }
    int index = _inventarView->currentRow();
    QListWidgetItem *item = _inventarView->takeItem(index);
    QStringList text = item->text().split('|');
    Player::gold += text[1].toInt();
    _money->setText(QString::number(Player::gold));
    Player::inventar->getItem(index);
    delete item;
}

void ItemshopGUI::buyItem(){
    if(_shopitemView->count() == 0){
        return;
    }

    QListWidgetItem *widgetitem = _shopitemView->currentItem();
    QStringList text = widgetitem->text().split('|');

    if(Player::gold >= text[1].toInt()){
        Player::gold -= text[1].toInt();
        _money->setText(QString::number(Player::gold));
        _inventarView->addItem(text[0] + '|' + text[1]);

        for(ItemObject *item : *_shopitemlist){
            if(item->getItemname().contains(text[0])){
                if(item->getType() == Itemtype::CONSUMABLE){
                    Player::inventar->addItem(new ConsumItem(*dynamic_cast<ConsumItem*>(item)));
                } else if (item->getType() == Itemtype::EGG){
                    Player::inventar->addItem(new ManaEgg(*dynamic_cast<ManaEgg*>(item)));
                } else if (item->getType() == Itemtype::GEAR){
                    Player::inventar->addItem(new GearItem(*dynamic_cast<GearItem*>(item)));
                } else if (item->getType() == Itemtype::MAGICSPELL){
                    Player::inventar->addItem(new MagicItem(*dynamic_cast<MagicItem*>(item)));
                }
            }
        }

    }

}

}
