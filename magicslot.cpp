#include "magicslot.h"

namespace fightsim {

    MagicSlot::MagicSlot(){ }

    MagicSlot::~MagicSlot() {
        for(auto n = 0; n < _spelllist.size(); n++){
            delete _spelllist.at(n);
        }
    }

    void MagicSlot::addSpellToList(MagicItem* spell){
        if(_spelllist.size() < 10){
            _spelllist.append(spell);
        }
    }

    MagicItem* MagicSlot::removeSpell(uint index){
        if(index < _spelllist.size()){
            MagicItem *temp = _spelllist.at(index);
            _spelllist.removeAt(index);
            return temp;
        }
        return nullptr;
    }

    MagicItem* MagicSlot::getSpell(uint index){
        return _spelllist.at(index);
    }

    QList<MagicItem*> MagicSlot::getSpelllist(){
        return _spelllist;
    }

}
