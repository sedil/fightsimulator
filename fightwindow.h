#ifndef FIGHTWINDOW_H
#define FIGHTWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QTextEdit>
#include <QBoxLayout>
#include <QLabel>
#include "player.h"
#include "npc.h"
#include "fight.h"

namespace fightsim {

class Fight;

/**
 * @brief The Fightwindow class
 * This class shows players and enemies statuspanel and shows informations
 * abput the fight
 */
class Fightwindow : public QWidget {
private:
    Q_OBJECT
    Fight *_fight;
    QList<Player*> *_player;
    QList<NPC*> *_npcs;
    QList<QLabel*> *_statusdisorder;
    QTextEdit *_fightinfo;
    QPushButton *_confirm;
    QPushButton *_abort;

    void initComponents();
    QGridLayout* createStatuspanel(const Actor *actor);
public:
    explicit Fightwindow(QList<Player*> *players, QList<NPC*> *opponents, QWidget *parent = nullptr);
    virtual ~Fightwindow();
    void updateActorpanel(Actor *actor, uint targetindex);
    void updateFightinfo(const QString &text);
    void fightEnd();
signals:

public slots:
    void commandInput();
    void abort();

};

}

#endif // FIGHTWINDOW_H
