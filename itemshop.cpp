#include "itemshop.h"
#include "consum.h"
#include "gear.h"
#include "magic.h"

namespace fightsim {

    ItemShop::ItemShop(Player *player) : _player(player){ }

    ItemShop::~ItemShop(){
        for(auto n = 0; n < _shopitems.size(); n++){
            delete _shopitems.at(n);
        }
    }

    void ItemShop::loadShopitems(QList<ItemObject*> &shopitems){
        _shopitems = shopitems;
    }

    void ItemShop::listShopitems() {
        QStringList items;
        for(ItemObject* item : _shopitems){
            items.append(item->getItemname() + " " + QString::number(item->getValue()));
        }
    }

    void ItemShop::listInventarItems() {
        QStringList items;
        QList<ItemObject*>* inventaritems = _player->accessInventar()->getInventar();
        for(ItemObject *item : *inventaritems){
            items.append(item->getItemname() + " " + QString::number(item->getValue()));
        }
    }

    void ItemShop::buyItem(uint index){
        if(index < _shopitems.size()){
            ItemObject *hold = _shopitems.at(index);
            if(_player->getGoldAmount() >= hold->getValue()){
                if(dynamic_cast<ConsumItem*>(hold)){
                    ConsumItem *cast = dynamic_cast<ConsumItem*>(hold);
                    ConsumItem *toInv = new ConsumItem(cast->getItemname(),cast->getItemdescription(),cast->getValue(),cast->getAttribute());
                    _player->accessInventar()->addItem(toInv);
                    _player->addGold(-toInv->getValue());
                } else if(dynamic_cast<GearItem*>(hold)){
                    GearItem *cast = dynamic_cast<GearItem*>(hold);
                    GearItem *toInv = new GearItem(cast->getItemname(),cast->getItemdescription(),cast->getGearType(),cast->getValue(),cast->getAttribute());
                    _player->accessInventar()->addItem(toInv);
                    _player->addGold(-toInv->getValue());
                } else if(dynamic_cast<MagicItem*>(hold)){
                    MagicItem *cast = dynamic_cast<MagicItem*>(hold);
                    MagicItem *toInv = new MagicItem(cast->getItemname(),cast->getItemdescription(),cast->getMagicType(),cast->getValue(),cast->getBasedamage(),cast->getMPUsage(),cast->isAOE(),cast->getAttribute());
                    _player->accessInventar()->addItem(toInv);
                    _player->addGold(-toInv->getValue());
                }
            }
        }
    }

    void ItemShop::sellItem(uint index){
        ItemObject *toSell = _player->accessInventar()->getItem(index);
        _player->addGold(toSell->getValue());
        delete toSell;
    }

}
