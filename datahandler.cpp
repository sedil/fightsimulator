#include "datahandler.h"

#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

#include "consum.h"
#include "gear.h"
#include "manaegg.h"
#include "magic.h"
#include "reward.h"
#include "npc.h"
#include "player.h"

namespace fightsim {

    DataHandler::DataHandler(const QString &path) {
        _configpath = path;
        loadData();
    }

    DataHandler::~DataHandler(){ }

    /**
     * @brief DataHandler::loadData
     * loads all necessary data
     */
    void DataHandler::loadData(){
        fileIsValid(_configpath);

        QFile itemdata = QFile(_configpath);
        itemdata.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray content = itemdata.readAll();
        itemdata.close();

        QJsonDocument jsondata = QJsonDocument::fromJson(content);
        QJsonObject paths = jsondata.object();

        _itemdatapath = paths["itemdata"].toString();
        _npcdatapath = paths["npcdata"].toString();
        _playerdatapath = paths["playerdata"].toString();

        fileIsValid(_itemdatapath);
        fileIsValid(_npcdatapath);
        fileIsValid(_playerdatapath);
    }

    bool DataHandler::fileIsValid(const QString &file){
        QFile data = QFile(file);
        if(!data.exists() | !data.open(QIODevice::ReadOnly)){
            return false;
        }
        return true;
    }

    /**
     * @brief DataHandler::defineMagictype
     * @param magictype magictype as string
     * @return enumeration of this magictype
     */
    Magictype DataHandler::defineMagictype(QString &magictype){
        if(magictype.contains("PHYS")){
            return Magictype::PHYS;
        } else if(magictype.contains("FIRE")){
            return Magictype::FIRE;
        } else if(magictype.contains("WIND")){
            return Magictype::WIND;
        } else if(magictype.contains("WATER")){
            return Magictype::WATER;
        } else if(magictype.contains("EARTH")){
            return Magictype::EARTH;
        } else if(magictype.contains("BOOST")){
            return Magictype::BOOST;
        } else if(magictype.contains("AILMENT")){
            return Magictype::AILMENT;
        } else {
            return Magictype::NO_ELEM;
        }
    }

    /**
     * @brief DataHandler::defineGeartype
     * @param geartype geartype as string
     * @return enumeration of this geartype
     */
    Geartype DataHandler::defineGeartype(QString &geartype){
        if(geartype.contains("HEAD")){
            return Geartype::HEAD;
        } else if (geartype.contains("CHEST")){
            return Geartype::CHEST;
        } else if (geartype.contains("FOOT")){
            return Geartype::FOOT;
        } else if (geartype.contains("WEAPON")){
            return Geartype::WEAPON;
        } else if (geartype.contains("MANAEGG")){
            return Geartype::MANAEGG;
        } else if (geartype.contains("ACCESSOIRE")){
            return Geartype::ACCESSOIRE;
        } else {
            return Geartype::NO_GEAR;
        }
    }

    /**
     * @brief DataHandler::createEffectPair
     * @param effect effect as string
     * @param value damage or healpower of this effect
     * @return pair of effect and damage
     */
    QPair<Effect, short> DataHandler::createEffectPair(QString effect, short value){
        if(effect.contains("REVIVE")){
            return QPair<Effect,short>(Effect::REVIVE,value);
        } else if(effect.contains("HEAL_HP")){
            return QPair<Effect,short>(Effect::HEAL_HP,value);
        } else if(effect.contains("HEAL_MP")){
            return QPair<Effect,short>(Effect::HEAL_MP,value);
        } else if(effect.contains("PHYS_DAMAGE")){
            return QPair<Effect,short>(Effect::PHYS_DAMAGE,value);
        } else if(effect.contains("FIRE_DAMAGE")){
            return QPair<Effect,short>(Effect::FIRE_DAMAGE,value);
        } else if(effect.contains("WIND_DAMAGE")){
            return QPair<Effect,short>(Effect::WIND_DAMAGE,value);
        } else if(effect.contains("WATER_DAMAGE")){
            return QPair<Effect,short>(Effect::WATER_DAMAGE,value);
        } else if(effect.contains("EARTH_DAMAGE")){
            return QPair<Effect,short>(Effect::EARTH_DAMAGE,value);
        } else if(effect.contains("ATK_UP")){
            return QPair<Effect,short>(Effect::ATK_UP,value);
        } else if(effect.contains("ATK_DOWN")){
            return QPair<Effect,short>(Effect::ATK_DOWN,value);
        } else if(effect.contains("DEF_UP")){
            return QPair<Effect,short>(Effect::DEF_UP,value);
        } else if(effect.contains("DEF_DOWN")){
            return QPair<Effect,short>(Effect::DEF_DOWN,value);
        } else if(effect.contains("ACT_UP")){
            return QPair<Effect,short>(Effect::ACT_UP,value);
        } else if(effect.contains("ACT_DOWN")){
            return QPair<Effect,short>(Effect::ACT_DOWN,value);
        } else if(effect.contains("POISON_ACTIVE")){
            return QPair<Effect,short>(Effect::POISON_ACTIVE,value);
        } else if(effect.contains("POISON_DEACTIVE")){
            return QPair<Effect,short>(Effect::POISON_DEACTIVE,value);
        } else if(effect.contains("PARALYZE_ACTIVE")){
            return QPair<Effect,short>(Effect::PARALYZE_ACTIVE,value);
        } else if(effect.contains("PARALYZE_DEACTIVE")){
            return QPair<Effect,short>(Effect::PARALYZE_DEACTIVE,value);
        } else if(effect.contains("MAGICBLOCK_ACTIVE")){
            return QPair<Effect,short>(Effect::MAGICBLOCK_ACTIVE,value);
        } else if(effect.contains("MAGICBLOCK_DEACTIVE")){
            return QPair<Effect,short>(Effect::MAGICBLOCK_DEACTIVE,value);
        } else {
            return QPair<Effect,short>(Effect::NO_EFFECT,0);
        }
    }

    /**
     * @brief DataHandler::saveProgress saves the progress in a JSON file
     * @param playerdata list of players
     */
    void DataHandler::saveProgress(const QList<Player*> *playerdata){
        QJsonArray root;
        for(Player *player : *playerdata){
            QJsonObject playerobject;
            playerobject.insert("name",player->getName());
            playerobject.insert("level",player->getLevel());
            playerobject.insert("hp",player->getMaximumHP());
            playerobject.insert("mp",player->getMaximumMP());
            playerobject.insert("str",player->getStrength());
            playerobject.insert("vit",player->getVitality());
            playerobject.insert("agi",player->getAgility());
            playerobject.insert("phys",player->getPhysResistance());
            playerobject.insert("fire",player->getFireResistance());
            playerobject.insert("wind",player->getWindResistance());
            playerobject.insert("water",player->getWaterResistance());
            playerobject.insert("earth",player->getEarthResistance());
            playerobject.insert("actXp",player->getActualXp());
            playerobject.insert("xpToLevelUp",player->getLevelXp());

            QList<MagicItem*> spells = player->accessMagicSlot()->getSpelllist();
            QJsonArray playerspells;
            for(MagicItem *spell : spells){
                playerspells.append(spell->getItemname());
            }
            playerobject.insert("spells",playerspells);

            QJsonObject playergear;
            QString head = player->accessGearSlot()->showHeadgear()->getItemname();
            playergear.insert("head",head);

            QString chest = player->accessGearSlot()->showChestgear()->getItemname();
            playergear.insert("chest",chest);

            QString foot = player->accessGearSlot()->showFootgear()->getItemname();
            playergear.insert("foot",foot);

            QString accessoire = player->accessGearSlot()->showAccessoire()->getItemname();
            playergear.insert("accessoire",accessoire);

            QString manaegg = player->accessGearSlot()->showManaegg()->getItemname();
            playergear.insert("manaegg",manaegg);

            QString weapon = player->accessGearSlot()->showWeapon()->getItemname();
            playergear.insert("weapon",weapon);

            playerobject.insert("gear",playergear);

            root.append(playerobject);
        }

        QJsonObject inventar;
        inventar.insert("gold",Player::gold);

        QJsonArray inventaritems;
        QList<ItemObject*> *temp = Player::inventar->getInventar();
        for(auto item : *temp){
            inventaritems.append(item->getItemname());
        }
        inventar.insert("inventar",inventaritems);
        root.append(inventar);

        QJsonDocument json;
        json.setArray(root);
        QFile data = QFile(_playerdatapath);
        data.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate);
        data.write(json.toJson());
        data.close();
    }

    /**
     * @brief DataHandler::getItemData
     * @return returns a list of all itemdata for the itemshop and
     * to compare itemnames
     */
    QList<ItemObject*>* DataHandler::getItemData(){
        QFile itemdata = QFile(_itemdatapath);
        itemdata.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray content = itemdata.readAll();
        itemdata.close();

        QJsonDocument jsondata = QJsonDocument::fromJson(content);
        QJsonArray data = jsondata.array();

        QList<ItemObject*> *itemlist = new QList<ItemObject*>();
        for(const QJsonValue &entry : data){
            QJsonObject object = entry.toObject();
            if(object["type"] == "CONSUMABLE"){
                QString name = object["name"].toString();
                QString desc = object["desc"].toString();
                int value = object["value"].toInt();
                QJsonObject attribute = object["attribute"].toObject();
                QString effect = attribute["effect"].toString();
                int effectvalue = attribute["value"].toInt();
                QPair<Effect, short> pair = createEffectPair(effect,effectvalue);

                ItemObject *consumitem = new ConsumItem(name,desc,value,pair);
                itemlist->append(consumitem);

            } else if(object["type"] == "GEAR"){
                QString name = object["name"].toString();
                QString desc = object["desc"].toString();
                QString geartype = object["geartype"].toString();
                int value = object["value"].toInt();
                QJsonObject attribute = object["attribute"].toObject();
                QString effect = attribute["effect"].toString();
                int effectvalue = attribute["value"].toInt();

                QPair<Effect, short> pair = createEffectPair(effect,effectvalue);
                Geartype gtype = defineGeartype(geartype);
                ItemObject *gearitem = new GearItem(name,desc,gtype,value,pair);

                itemlist->append(gearitem);

            } else if(object["type"] == "EGG"){
                QString name = object["name"].toString();
                QString desc = object["desc"].toString();
                int value = object["value"].toInt();
                int fireboost = object["fireboost"].toInt();
                int windboost = object["windboost"].toInt();
                int waterboost = object["waterboost"].toInt();
                int earthboost = object["earthboost"].toInt();
                ItemObject *egg = new ManaEgg(name,desc,fireboost,windboost,waterboost,earthboost,value);
                itemlist->append(egg);

            } else if(object["type"] == "MAGICSPELL"){
                QString name = object["name"].toString();
                QString desc = object["desc"].toString();
                QString magictype = object["magictype"].toString();
                int value = object["value"].toInt();
                QJsonObject attribute = object["attribute"].toObject();
                QString effect = attribute["effect"].toString();
                int effectvalue = attribute["value"].toInt();
                int basedamage = attribute["basedamage"].toInt();
                int mp = attribute["mp"].toInt();
                bool aoe = attribute["aoe"].toBool();
                qDebug() << name << " " << aoe;

                QPair<Effect, short> pair = createEffectPair(effect,effectvalue);
                Magictype mtype = defineMagictype(magictype);
                ItemObject *magicitem = new MagicItem(name,desc,mtype,value,basedamage,mp,aoe,pair);
                itemlist->append(magicitem);
            } else {
                qDebug() << "error";
            }
        }
        return itemlist;
    }

    /**
     * @brief DataHandler::getPlayers
     * @param itemlist list of items to help to add items to the playerinventar
     * @return list of players
     */
    QList<Player*>* DataHandler::getPlayers(const QList<ItemObject*> *itemlist){
        QFile playerdata = QFile(_playerdatapath);
        playerdata.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray content = playerdata.readAll();
        playerdata.close();

        QJsonDocument jsondata = QJsonDocument::fromJson(content);
        QJsonArray data = jsondata.array();

        QList<Player*> *playerlist = new QList<Player*>();

        QMap<ushort,uint> xpMap = QMap<ushort,uint>();
        uint basexp = 30;
        for(ushort n = 2; n <= 100; n++){
            if(n <= 20){
                basexp*=1.25;
            } else if(n>20 && n<=50){
                basexp*=1.2;
            } else if(n>50 && n<=70){
                basexp*=1.05;
            } else if(n>70){
                basexp*=1.02;
            }
            xpMap.insert(n,static_cast<uint>(basexp));
        }
        Player::xpMap = xpMap;

        for(const QJsonValue &entry : data){
            QString name = entry["name"].toString();
            int level = entry["level"].toInt();
            int hp = entry["hp"].toInt();
            int mp = entry["mp"].toInt();
            int str = entry["str"].toInt();
            int vit = entry["vit"].toInt();
            int agi = entry["agi"].toInt();
            int phys = entry["phys"].toInt();
            int fire = entry["fire"].toInt();
            int wind = entry["wind"].toInt();
            int water = entry["water"].toInt();
            int earth = entry["earth"].toInt();
            int actXp = entry["actXp"].toInt();
            int xpToLevelup = entry["xpToLevelUp"].toInt();
            QJsonArray spells = entry["spells"].toArray();

            QList<MagicItem*> spelllist;
            if(!spells.isEmpty()){
                for(auto n = 0; n < spells.size(); n++){
                    for(ItemObject *spell : *itemlist){
                        if(spell->getType() == Itemtype::MAGICSPELL && spell->getItemname().contains(spells[n].toString())){
                            MagicItem *magicspell = new MagicItem(*dynamic_cast<MagicItem*>(spell));
                            spelllist.append(magicspell);
                            break;
                        }
                    }
                }
            }

            Player *player = new Player(name,level,hp,mp,str,vit,agi,phys,fire,wind,water,earth,actXp,xpToLevelup);
            for(MagicItem *spell : spelllist){
                player->accessMagicSlot()->addSpellToList(spell);
            }

            QJsonObject gear = entry["gear"].toObject();
            QString headgear = gear["head"].toString();
            for(ItemObject *gear : *itemlist){
                if(gear->getType() == Itemtype::GEAR && gear->getItemname().contains(headgear)){
                    player->accessGearSlot()->initHeadgear(new GearItem(*dynamic_cast<GearItem*>(gear)));
                    break;
                }
            }

            QString chestgear = gear["chest"].toString();
            for(ItemObject *gear : *itemlist){
                if(gear->getType() == Itemtype::GEAR && gear->getItemname().contains(chestgear)){
                    player->accessGearSlot()->initChestgear(new GearItem(*dynamic_cast<GearItem*>(gear)));
                    break;
                }
            }

            QString footgear = gear["foot"].toString();
            for(ItemObject *gear : *itemlist){
                if(gear->getType() == Itemtype::GEAR && gear->getItemname().contains(footgear)){
                    player->accessGearSlot()->initFootgear(new GearItem(*dynamic_cast<GearItem*>(gear)));
                    break;
                }
            }

            QString accessoire = gear["accessoire"].toString();
            for(ItemObject *gear : *itemlist){
                if(gear->getType() == Itemtype::GEAR && gear->getItemname().contains(accessoire)){
                    player->accessGearSlot()->initAccessoire(new GearItem(*dynamic_cast<GearItem*>(gear)));
                    break;
                }
            }

            QString manaegg = gear["manaegg"].toString();
            for(ItemObject *gear : *itemlist){
                if(gear->getType() == Itemtype::EGG && gear->getItemname().contains(manaegg)){
                    player->accessGearSlot()->initManaEgg(new ManaEgg(*dynamic_cast<ManaEgg*>(gear)));
                    break;
                }
            }

            QString weapon = gear["weapon"].toString();
            for(ItemObject *gear : *itemlist){
                if(gear->getType() == Itemtype::GEAR && gear->getItemname().contains(weapon)){
                    player->accessGearSlot()->initWeapon(new GearItem(*dynamic_cast<GearItem*>(gear)));
                    break;
                }
            }
            playerlist->append(player);
            if(playerlist->size() == 3){
                break;
            }
        }

        QJsonObject inventar = data[3].toObject();
        Player::gold = inventar["gold"].toInt();
        Player::inventar = new Inventar();

        QJsonArray inventaritems = inventar["inventar"].toArray();
        for(const QJsonValueRef &entry : inventaritems){
            for(ItemObject *item : *itemlist){
                if(item->getItemname().contains(entry.toString())){
                    if(item->getType() == Itemtype::CONSUMABLE){
                        Player::inventar->addItem(new ConsumItem(*dynamic_cast<ConsumItem*>(item)));
                        break;
                    } else if(item->getType() == Itemtype::EGG){
                        Player::inventar->addItem(new ManaEgg(*dynamic_cast<ManaEgg*>(item)));
                        break;
                    } else if(item->getType() == Itemtype::GEAR){
                        Player::inventar->addItem(new GearItem(*dynamic_cast<GearItem*>(item)));
                        break;
                    } else if(item->getType() == Itemtype::MAGICSPELL){
                        Player::inventar->addItem(new MagicItem(*dynamic_cast<MagicItem*>(item)));
                        break;
                    }
                }
            }
        }

        return playerlist;
    }

    /**
     * @brief DataHandler::getNPCs
     * @param itemlist list of items to help to add rewards from this npc
     * @return list of all available npcs
     */
    QList<NPC*>* DataHandler::getNPCs(const QList<ItemObject*> *itemlist){
        QFile npcdata = QFile(_npcdatapath);
        npcdata.open(QIODevice::ReadOnly | QIODevice::Text);
        QByteArray content = npcdata.readAll();
        npcdata.close();

        QJsonDocument jsondata = QJsonDocument::fromJson(content);
        QJsonArray data = jsondata.array();

        QList<NPC*>* npclist = new QList<NPC*>();
        for(const QJsonValue &entry : data){
            QString name = entry["name"].toString();
            int level = entry["level"].toInt();
            int hp = entry["hp"].toInt();
            int mp = entry["mp"].toInt();
            int str = entry["str"].toInt();
            int vit = entry["vit"].toInt();
            int agi = entry["agi"].toInt();
            int phys = entry["phys"].toInt();
            int fire = entry["fire"].toInt();
            int wind = entry["wind"].toInt();
            int water = entry["water"].toInt();
            int earth = entry["earth"].toInt();
            QJsonArray spells = entry["spells"].toArray();

            QList<MagicItem*> spelllist;
            if(!spells.isEmpty()){
                for(auto n = 0; n < spells.size(); n++){
                    for(ItemObject *spell : *itemlist){
                        if(spell->getType() == Itemtype::MAGICSPELL && spell->getItemname().contains(spells[n].toString())){
                            MagicItem *magicspell = new MagicItem(*dynamic_cast<MagicItem*>(spell));
                            spelllist.append(magicspell);
                            break;
                        }
                    }
                }
            }

            QJsonObject rewards = entry["rewards"].toObject();
            int xp = rewards["xp"].toInt();
            int gold = rewards["gold"].toInt();
            if(rewards["item"].isNull()){
                Reward reward(xp,gold,nullptr);
                NPC *npc = new NPC(name,level,hp,mp,str,vit,agi,phys,fire,wind,water,earth,reward);

                for(MagicItem *spell : spelllist){
                    npc->accessMagicSlot()->addSpellToList(spell);
                }

                npclist->append(npc);
            } else {
                QString itemname = rewards["item"].toString();
                ItemObject *copy = nullptr;
                for(ItemObject *item : *itemlist){
                    if(item->getItemname().contains(itemname)){
                        if(item->getType() == Itemtype::CONSUMABLE){
                            copy = new ConsumItem(*dynamic_cast<ConsumItem*>(item));
                        } else if (item->getType() == Itemtype::GEAR){
                            copy = new GearItem(*dynamic_cast<GearItem*>(item));
                        } else if (item->getType() == Itemtype::MAGICSPELL){
                            copy = new MagicItem(*dynamic_cast<MagicItem*>(item));
                        }
                        break;
                    }
                }

                Reward reward(xp,gold,copy);
                NPC *npc = new NPC(name,level,hp,mp,str,vit,agi,phys,fire,wind,water,earth,reward);

                for(MagicItem *spell : spelllist){
                    npc->accessMagicSlot()->addSpellToList(spell);
                }

                npclist->append(npc);
            }
        }
        return npclist;
    }

}
