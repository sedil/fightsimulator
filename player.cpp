#include "player.h"

namespace fightsim {

    Player::Player(QString name, ushort level, ushort hp, ushort mp, ushort str, ushort vit, ushort agi, short phys, short fire, short wind, short water, short earth, ushort xp, ushort maxXP) :
        Actor(name,level,hp,mp,str,vit,agi,phys,fire,wind,water,earth,false), _actXp(xp), _maxXp(maxXP) {
        _equipment = new GearSlot(this);
    }

    Player::~Player() {
        delete _equipment;
    }

    /**
     * @brief Player::addXp adds xp to this player
     * @param xp amount of xp added
     */
    void Player::addXp(ushort xp){
        if(_actXp + xp >= _maxXp){
            ushort overflow = _actXp + xp - _maxXp;
            _actXp = overflow;
            levelUp();
        } else {
            _actXp += xp;
        }
    }

    void Player::changeStrength(short str){
        _strength += str;
    }

    void Player::changeVitality(short vit){
        _vitality += vit;
    }

    void Player::changeAgility(short agi){
        _agility += agi;
    }

    /**
     * @brief Player::levelUp levels one player up
     */
    void Player::levelUp(){
        if(_level < 100){
            _level++;
            _maxXp = xpMap[_level+1];

            _maxHP=static_cast<ushort>(_maxHP*1.15);
            _maxMP=static_cast<ushort>(_maxMP*1.1);
            _strength+=4;
            _vitality+=3;
            _agility+=2;
        }
    }

    /**
     * @brief Player::addGold add gold to the playergroups inventar
     * @param change amount which will be added
     */
    void Player::addGold(short change) {
        if(gold + change >= 0){
            gold += change;
        }
    }

    ushort Player::getActualXp() const {
        return _actXp;
    }

    ushort Player::getLevelXp() const {
        return _maxXp;
    }

    ushort Player::getGoldAmount() const {
        return gold;
    }

    GearSlot* Player::accessGearSlot() const {
        return _equipment;
    }

    Inventar* Player::accessInventar() {
        return inventar;
    }
}
