#include "npc.h"

namespace fightsim {

    NPC::NPC(QString name, ushort level, ushort hp, ushort mp, ushort str, ushort vit, ushort agi, short phys, short fire, short wind, short water, short earth, Reward reward) :
        Actor(name,level,hp,mp,str,vit,agi,phys,fire,wind,water,earth,true), _reward(reward){ }

    NPC::~NPC() { }

    /**
     * @brief NPC::getReward
     * @return returns npcs reward
     */
    Reward NPC::getReward() const {
        return _reward;
    }

}
