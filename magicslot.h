#ifndef MAGICSLOT_H
#define MAGICSLOT_H

#include <QObject>
#include <QList>
#include "magic.h"

namespace fightsim {

    /**
     * @brief The MagicSlot class
     * Helperclass which contains a list of spells for the actors
     */
    class MagicSlot {
    private:
        QList<MagicItem*> _spelllist;
    public:
        explicit MagicSlot();
        virtual ~MagicSlot();

        void addSpellToList(MagicItem* spell);
        MagicItem* removeSpell(uint index);
        MagicItem* getSpell(uint index);
        QList<MagicItem*> getSpelllist();
    };

}

#endif // MAGICSLOT_H
